import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_list/crypto_list_bloc.dart';
import 'package:crypto_app/features/events/presentation/bloc/news_bloc.dart';
import 'package:crypto_app/features/favorites/presentation/bloc/favorites_bloc.dart';
import 'package:crypto_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:crypto_app/features/user/presentation/bloc/user_bloc.dart';
import 'package:crypto_app/features/user/presentation/pages/user_check_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'features/crypto_list/presentation/bloc/crypto_history/crypto_history_bloc.dart';
import 'features/on_boarding/bloc/on_boarding_bloc.dart';
import 'features/on_boarding/pages/on_boarding_page.dart';
import 'injection_container.dart' as di;
import 'injection_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await di.init();
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('en'), Locale('hu')],
      path: 'lib/core/assets/translations',
      fallbackLocale: const Locale('en'),
      child: CryptoApp(),
    ),
  );
}

class CryptoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<OnBoardingBloc>(
          create: (_) => sl<OnBoardingBloc>(),
        ),
        BlocProvider<CryptoListBloc>(
          create: (_) => sl<CryptoListBloc>(),
        ),
        BlocProvider<NewsBloc>(
          create: (_) => sl<NewsBloc>(),
        ),
        BlocProvider<LoginBloc>(
          create: (_) => sl<LoginBloc>(),
        ),
        BlocProvider<UserBloc>(
          create: (_) => sl<UserBloc>(),
        ),
        BlocProvider<FavoritesBloc>(
          create: (_) => sl<FavoritesBloc>(),
        ),
        BlocProvider<CryptoHistoryBloc>(
          create: (_) => sl<CryptoHistoryBloc>(),
        ),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        title: KStrings.appName,
        theme: ThemeData(
          primaryColor: KColors.appPurple,
          primarySwatch: KColors.appPurple,
        ),
        home: Scaffold(
          body: BlocBuilder<OnBoardingBloc, OnBoardingState>(
            bloc: sl<OnBoardingBloc>(),
            builder: (context, state) {
              if (state is ShouldShowApp) {
                return UserCheckPage();
              }
              if (state is ShouldShowOnBoarding) {
                return OnBoarding();
              }
              if (state is EmptyOnBoarding) {
                sl<OnBoardingBloc>().add(CheckIfShouldShowOnBoarding());
                return const Center(
                    child: CircularProgressIndicator.adaptive());
              }
              return const Center(
                child: CircularProgressIndicator.adaptive(),
              );
            },
          ),
        ),
      ),
    );
  }
}
