import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/local/crypto_list_local_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/remote/crypto_history_remote_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/remote/crypto_list_remote_data_source.dart';
import 'package:crypto_app/features/crypto_list/domain/repositories/crypto_list_repository.dart';
import 'package:crypto_app/features/crypto_list/domain/use_cases/get_history_use_case.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_list/crypto_list_bloc.dart';
import 'package:crypto_app/features/events/data/data_sources/local/news_local_data_source.dart';
import 'package:crypto_app/features/events/data/data_sources/remote/news_remote_data_source.dart';
import 'package:crypto_app/features/events/data/repositories/news_repository_impl.dart';
import 'package:crypto_app/features/events/domain/repositories/news_repository.dart';
import 'package:crypto_app/features/events/domain/use_cases/get_news_use_case.dart';
import 'package:crypto_app/features/events/presentation/bloc/news_bloc.dart';
import 'package:crypto_app/features/favorites/data/data_sources/local/favorites_local_data_source.dart';
import 'package:crypto_app/features/favorites/domain/repositories/favorites_repository.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/add_to_favorites_use_case.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/get_favorites_use_case.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/remove_from_favorites_use_case.dart';
import 'package:crypto_app/features/favorites/presentation/bloc/favorites_bloc.dart';
import 'package:crypto_app/features/login/data/data_sources/local/login_local_data_source.dart';
import 'package:crypto_app/features/login/domain/repositories/login_repository.dart';
import 'package:crypto_app/features/login/domain/use_cases/verify_sms_use_case.dart';
import 'package:crypto_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:crypto_app/features/user/domain/use_cases/get_user_use_case.dart';
import 'package:crypto_app/features/user/domain/use_cases/log_out_use_case.dart';
import 'package:crypto_app/features/user/presentation/bloc/user_bloc.dart';
import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'core/network_info.dart';
import 'features/crypto_list/data/data_sources/local/crypto_history_local_data_source.dart';
import 'features/crypto_list/data/repositories/crypto_list_repository_impl.dart';
import 'features/crypto_list/domain/use_cases/get_crypto_list_use_case.dart';
import 'features/crypto_list/domain/use_cases/search_crypto.dart';
import 'features/crypto_list/presentation/bloc/crypto_history/crypto_history_bloc.dart';
import 'features/favorites/data/data_sources/remote/favorites_remote_data_source.dart';
import 'features/favorites/data/repositories/favorites_repository_impl.dart';
import 'features/login/data/repositories/login_repository_impl.dart';
import 'features/login/domain/use_cases/authenticate_phone_use_case.dart';
import 'features/on_boarding/bloc/on_boarding_bloc.dart';
import 'package:http/http.dart' as http;

import 'features/user/data/data_sources/local/user_local_data_source.dart';
import 'features/user/data/repositories/user_repository_impl.dart';
import 'features/user/domain/repositories/user_repository.dart';

final sl = GetIt.instance;

Future<void> init() async {
  final app = await Firebase.initializeApp();
  //bloc
  sl.registerLazySingleton(
    () => OnBoardingBloc(),
  );

  sl.registerLazySingleton(
    () => CryptoListBloc(
      getCryptoList: sl(),
      searchCrypto: sl(),
    ),
  );

  sl.registerLazySingleton(
    () => NewsBloc(
      getNews: sl(),
    ),
  );

  sl.registerLazySingleton(
    () => LoginBloc(
      authenticatePhone: sl(),
      verifySms: sl(),
    ),
  );

  sl.registerLazySingleton(
    () => UserBloc(
      getUser: sl(),
      logOut: sl(),
    ),
  );

  sl.registerLazySingleton(
    () => FavoritesBloc(
      getFavorites: sl(),
      addToFavorites: sl(),
      removeFromFavorites: sl(),
    ),
  );

  sl.registerLazySingleton(
    () => CryptoHistoryBloc(
      getHistory: sl(),
    ),
  );

  //repository
  sl.registerLazySingleton<CryptoListRepository>(
    () => CryptoListRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
      localDataSource: sl(),
      historyLocalDataSource: sl(),
      historyRemoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<LoginRepository>(
    () => LoginRepositoryImpl(
      networkInfo: sl(),
      auth: sl(),
      localDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<NewsRepository>(
    () => NewsRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
      localDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      networkInfo: sl(),
      localDataSource: sl(),
      auth: sl(),
    ),
  );

  sl.registerLazySingleton<FavoritesRepository>(
    () => FavoritesRepositoryImpl(
      networkInfo: sl(),
      localDataSource: sl(),
      remoteDataSource: sl(),
    ),
  );

  //usecase
  sl.registerLazySingleton(() => GetCrypoListUseCase(sl()));
  sl.registerLazySingleton(() => GetNewsUseCase(sl()));
  sl.registerLazySingleton(() => AuthenticatePhoneUseCase(sl()));
  sl.registerLazySingleton(() => VerifySmsUseCase(sl()));
  sl.registerLazySingleton(() => GetUserUseCase(sl()));
  sl.registerLazySingleton(() => LogOutUseCase(sl()));
  sl.registerLazySingleton(() => GetFavoritesUseCase(sl()));
  sl.registerLazySingleton(() => AddToFavoritesUseCase(sl()));
  sl.registerLazySingleton(() => RemoveFromFavoritesUseCase(sl()));
  sl.registerLazySingleton(() => GetHistoryUseCase(sl()));
  sl.registerLazySingleton(() => SearchCryptoUseCase(sl()));

  //dataSource
  sl.registerLazySingleton<CryptoListRemoteDataSource>(
    () => CryptoListRemoteDataSourceImpl(client: sl()),
  );
  sl.registerLazySingleton<CryptoListLocalDataSource>(
    () => CryptoListLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<NewsRemoteDataSource>(
    () => NewsRemoteDataSourceImpl(client: sl()),
  );
  sl.registerLazySingleton<NewsLocalDataSource>(
    () => NewsLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<LoginLocalDataSource>(
    () => LoginLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<UserLocalDataSource>(
    () => UserLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<FavoritesLocalDataSource>(
    () => FavoritesLocalDataSourceImpl(sharedPreferences: sl()),
  );
  sl.registerLazySingleton<FavoritesRemoteDataSource>(
    () => FavoritesRemoteDataSourceImpl(firebaseDatabase: sl()),
  );

  sl.registerLazySingleton<CryptoHistoryRemoteDataSource>(
    () => CryptoHistoryRemoteDataSourceImpl(client: sl()),
  );

  sl.registerLazySingleton<CryptoHistoryLocalDataSource>(
    () => CryptoHistoryLocalDataSourceImpl(sharedPreferences: sl()),
  );

  //network
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //external
  final sharedPreferences = await SharedPreferences.getInstance();
  final firebaseaAuth = FirebaseAuth.instance;
  final firebaseDatabase = FirebaseDatabase(
    app: app,
    databaseURL: KUrls.databaseUrl,
  );
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => firebaseDatabase);
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => firebaseaAuth);
  sl.registerLazySingleton(() => http.Client());
}
