part of 'user_bloc.dart';


abstract class UserState extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyUser extends UserState {}

class LoadingUser extends UserState {}

class UserLoggedOut extends UserState {}

class LoadedUser extends UserState {
  final Map<String, String> user;

  LoadedUser({required this.user});

  @override
  List<Object> get props => [user];
}

class NoUserRegistered extends UserState {}