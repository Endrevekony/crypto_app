import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/user/domain/use_cases/get_user_use_case.dart';
import 'package:crypto_app/features/user/domain/use_cases/log_out_use_case.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final GetUserUseCase getUser;
  final LogOutUseCase logOut;
  UserBloc({required this.getUser, required this.logOut}) : super(EmptyUser());

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is GetUser) {
      yield LoadingUser();
      final failureOrUser = await getUser(NoParams());
      yield* _eitherUserOrErrorState(failureOrUser);
    }
    if (event is LogOut) {
      await logOut();
      yield UserLoggedOut();
    }
  }

  Stream<UserState> _eitherUserOrErrorState(
    Either<Failure, Map<String, String>> either,
  ) async* {
    yield either.fold(
      (failure) => NoUserRegistered(),
      (user) => LoadedUser(user: user),
    );
  }
}
