import 'package:crypto_app/core/widgets/main_navigation_page.dart.dart';
import 'package:crypto_app/features/login/presentation/pages/phone_verification_page.dart';
import 'package:crypto_app/features/user/presentation/bloc/user_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class UserCheckPage extends StatefulWidget {
  @override
  _UserCheckPageState createState() => _UserCheckPageState();
}

class _UserCheckPageState extends State<UserCheckPage>
    with AutomaticKeepAliveClientMixin<UserCheckPage> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<UserBloc, UserState>(
      bloc: sl<UserBloc>(),
      builder: (context, state) {
        if (state is EmptyUser) {
          sl<UserBloc>().add(GetUser());
        }
        if (state is LoadedUser) {
          return MainNavigationPage(
            userPhoneNumber: state.user.values.first,
            userId: state.user.keys.first,
          );
        }
        if (state is UserLoggedOut) {
          return PhoneVerificationPage();
        }
        if (state is NoUserRegistered) {
          return PhoneVerificationPage();
        }
        return const Center(
          child: CircularProgressIndicator.adaptive(),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
