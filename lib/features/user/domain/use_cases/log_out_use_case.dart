import 'package:crypto_app/features/user/domain/repositories/user_repository.dart';

class LogOutUseCase{
  final UserRepository repository;

  LogOutUseCase(this.repository);

  Future<void> call() async {
    return await repository.logOut();
  }
}
