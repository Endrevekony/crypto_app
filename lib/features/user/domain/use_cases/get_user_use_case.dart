import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/user/domain/repositories/user_repository.dart';
import 'package:dfunc/dfunc.dart';

class GetUserUseCase implements UseCase<Map<String, String>, NoParams> {
  final UserRepository repository;

  GetUserUseCase(this.repository);

  @override
  Future<Either<Failure, Map<String, String>>> call(NoParams params) async {
    return await repository.getUser();
  }
}
