import 'package:crypto_app/core/errors/failures.dart';
import 'package:dfunc/dfunc.dart';

abstract class UserRepository {
  Future<Either<Failure, Map<String, String>>> getUser();
  Future<void> logOut();

}
