import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/network_info.dart';
import 'package:crypto_app/features/user/data/data_sources/local/user_local_data_source.dart';
import 'package:crypto_app/features/user/domain/repositories/user_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserRepositoryImpl implements UserRepository {
  final UserLocalDataSource localDataSource;
  final FirebaseAuth auth;
  final NetworkInfo networkInfo;

  UserRepositoryImpl({
    required this.localDataSource,
    required this.networkInfo,
    required this.auth,
  });

  @override
  Future<Either<Failure, Map<String, String>>> getUser() async {
    try {
      final result = await localDataSource.getLastUser();
      return Either.right(result);
    } on CacheException {
      return Either.left(CacheFailure());
    }
  }

  @override
  Future<void> logOut() async {
    if(await networkInfo.isConnected){
      await auth.signOut();
    }
    await localDataSource.logOut();
  }
}