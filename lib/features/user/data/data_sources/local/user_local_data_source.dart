import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserLocalDataSource {
  Future<Map<String, String>> getLastUser();

  Future<void> logOut();
}

const cachedPhoneNumber = 'CACHED_PHONE_NUMBER';
const cachedUserId = 'CACHED_USER_ID';

class UserLocalDataSourceImpl implements UserLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<Map<String, String>> getLastUser() {
    final userPhoneNumber = sharedPreferences.getString(cachedPhoneNumber);
    final userId = sharedPreferences.getString(cachedUserId);
    if (userId != null && userPhoneNumber != null) {
      return Future.value({userId : userPhoneNumber});
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> logOut() async{
    sharedPreferences.remove(cachedPhoneNumber);
    sharedPreferences.remove(cachedUserId);
  }
}