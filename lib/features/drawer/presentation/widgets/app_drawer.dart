import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/user/presentation/bloc/user_bloc.dart';
import 'package:crypto_app/main.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../injection_container.dart';

class AppDrawer extends StatelessWidget {
  final String userPhoneNumber;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const AppDrawer({required this.userPhoneNumber, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        shrinkWrap: true,
        children: [
          ListTile(
            title: Center(child: Text(userPhoneNumber)),
          ),
          ListTile(
            title: Center(
              child: Text(KStrings.languages.tr()),
            ),
          ),
          ListTile(
            leading: Flag.fromCode(
              FlagsCode.GB,
              width: 30,
              height: 30,
            ),
            title: const Text(KStrings.english),
            onTap: () async {
              Navigator.of(context).pop();
              await context.setLocale(const Locale('en'));
            },
          ),
          ListTile(
            leading: Flag.fromCode(
              FlagsCode.HU,
              width: 30,
              height: 30,
            ),
            title: const Text(KStrings.hungarian),
            onTap: () {
              Navigator.of(context).pop();
              context.setLocale(const Locale('hu'));
            },
          ),
          ListTile(
            leading: const Icon(
              FontAwesomeIcons.signOutAlt,
              color: KColors.darkPurple,
            ),
            title: Text(KStrings.logOut.tr()),
            onTap: () {
              sl<UserBloc>().add(LogOut());
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => CryptoApp(),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
