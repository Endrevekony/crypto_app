
class DateInformation {
  DateInformation({
    required this.volume,
    required this.priceChange,
    required this.priceChangePct,
    required this.volumeChange,
    required this.volumeChangePct,
    required this.marketCapChange,
    required this.marketCapChangePct,
  });

  final String volume;
  final String priceChange;
  final String priceChangePct;
  final String volumeChange;
  final String volumeChangePct;
  final String marketCapChange;
  final String marketCapChangePct;

  factory DateInformation.fromJson(Map<String, dynamic> json) => DateInformation(
    volume: json["volume"] ?? "",
    priceChange: json["price_change"] ?? "",
    priceChangePct: json["price_change_pct"] ?? "",
    volumeChange: json["volume_change"] ?? "",
    volumeChangePct: json["volume_change_pct"] ?? "",
    marketCapChange: json["market_cap_change"] ?? "",
    marketCapChangePct: json["market_cap_change_pct"] ?? "",
  );

  Map<String, dynamic> toJson() => {
    "volume": volume,
    "price_change": priceChange,
    "price_change_pct": priceChangePct,
    "volume_change": volumeChange,
    "volume_change_pct": volumeChangePct,
    "market_cap_change": marketCapChange,
    "market_cap_change_pct": marketCapChangePct,
  };
}
