import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:dfunc/dfunc.dart';

abstract class CryptoListRepository {
  Future<Either<Failure, List<CryptoItemModel>>> getAllCryptos(
      {required int page});
  Future<Either<Failure, List<CryptoHistoryModel>>> getHistory({
    required String cryptoName,
    required bool isUsd,
    required DateTime date,
  });
  Future<Either<Failure, List<CryptoItemModel>>> search({required String keyWord});
}
