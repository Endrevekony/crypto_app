import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:crypto_app/features/crypto_list/domain/repositories/crypto_list_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class GetHistoryUseCase
    implements UseCase<List<CryptoHistoryModel>, CryptoHistoryParams> {
  final CryptoListRepository repository;

  GetHistoryUseCase(this.repository);

  @override
  Future<Either<Failure, List<CryptoHistoryModel>>> call(
      CryptoHistoryParams params) async {
    return await repository.getHistory(
        isUsd: params.isUsd, cryptoName: params.cryptoName, date: params.date);
  }
}

class CryptoHistoryParams extends Equatable {
  final String cryptoName;
  final bool isUsd;
  final DateTime date;

  const CryptoHistoryParams(
      {required this.cryptoName, required this.isUsd, required this.date});

  @override
  List<Object> get props => [cryptoName, isUsd, date];
}
