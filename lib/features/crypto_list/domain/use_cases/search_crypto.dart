import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/domain/repositories/crypto_list_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class SearchCryptoUseCase
    implements UseCase<List<CryptoItemModel>, SearchParams> {
  final CryptoListRepository repository;

  SearchCryptoUseCase(this.repository);

  @override
  Future<Either<Failure, List<CryptoItemModel>>> call(
      SearchParams params) async {
    return await repository.search(keyWord: params.keyword,);
  }
}

class SearchParams extends Equatable {
  final String keyword;

  const SearchParams({
    required this.keyword,
  });

  @override
  List<Object> get props => [keyword];
}
