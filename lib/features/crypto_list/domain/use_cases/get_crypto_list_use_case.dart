import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/domain/repositories/crypto_list_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class GetCrypoListUseCase
    implements UseCase<List<CryptoItemModel>, CryptoListParams> {
  final CryptoListRepository repository;

  GetCrypoListUseCase(this.repository);

  @override
  Future<Either<Failure, List<CryptoItemModel>>> call(
      CryptoListParams params) async {
    return await repository.getAllCryptos(page: params.page,);
  }
}

class CryptoListParams extends Equatable {
  final int page;

  const CryptoListParams({
    required this.page
  });

  @override
  List<Object> get props => [page];
}
