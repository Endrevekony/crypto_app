
import 'dart:convert';

import 'package:crypto_app/features/crypto_list/domain/entities/date_information.dart';

List<CryptoItemModel> cryptoItemModelFromJson(String str) => List<CryptoItemModel>.from(json.decode(str).map((x) => CryptoItemModel.fromJson(x)));

String cryptoItemModelToJson(List<CryptoItemModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CryptoItemModel {
  CryptoItemModel({
    required this.id,
    required this.currency,
    required this.symbol,
    required this.name,
    required this.logoUrl,
    required this.status,
    required this.price,
    required this.priceDate,
    required this.priceTimestamp,
    required this.circulatingSupply,
    required this.maxSupply,
    required this.marketCap,
    required this.marketCapDominance,
    required this.numExchanges,
    required this.numPairs,
    required this.numPairsUnmapped,
    required this.firstCandle,
    required this.firstTrade,
    required this.firstOrderBook,
    required this.rank,
    required this.rankDelta,
    required this.high,
    required this.highTimestamp,
    required this.the1D,
    required this.the7D,
    required this.the30D,
    required this.the365D,
    required this.ytd,
  });

  final String id;
  final String currency;
  final String symbol;
  final String name;
  final String logoUrl;
  final String status;
  final String price;
  final DateTime? priceDate;
  final DateTime? priceTimestamp;
  final String circulatingSupply;
  final String maxSupply;
  final String marketCap;
  final String marketCapDominance;
  final String numExchanges;
  final String numPairs;
  final String numPairsUnmapped;
  final String firstCandle;
  final String firstTrade;
  final String firstOrderBook;
  final String rank;
  final String rankDelta;
  final String high;
  final String highTimestamp;
  final DateInformation? the1D;
  final DateInformation? the7D;
  final DateInformation? the30D;
  final DateInformation? the365D;
  final DateInformation? ytd;

  factory CryptoItemModel.fromJson(Map<String, dynamic> json) => CryptoItemModel(
    id: json["id"],
    currency: json["currency"],
    symbol: json["symbol"],
    name: json["name"],
    logoUrl: json["logo_url"] ?? "",
    status: json["status"] ?? "",
    price: json["price"] ?? "",
    priceDate: DateTime?.parse(json["price_date"] ?? DateTime.now().toIso8601String()),
    priceTimestamp: DateTime?.parse(json["price_timestamp"] ?? DateTime.now().toIso8601String()),
    circulatingSupply: json["circulating_supply"] ?? "",
    maxSupply: json["max_supply"] ?? "",
    marketCap: json["market_cap"] ?? "",
    marketCapDominance: json["market_cap_dominance"] ?? "",
    numExchanges: json["num_exchanges"] ?? "",
    numPairs: json["num_pairs"] ?? "",
    numPairsUnmapped: json["num_pairs_unmapped"] ?? "",
    firstCandle: json["first_candle"] ?? "",
    firstTrade: json["first_trade"] ?? "",
    firstOrderBook: json["first_order_book"] ?? "",
    rank: json["rank"] ?? "",
    rankDelta: json["rank_delta"] ?? "",
    high: json["high"] ?? "",
    highTimestamp: json["high_timestamp"] ?? "",
    the1D: DateInformation.fromJson(json["1d"] ?? {"": ""}),
    the7D: DateInformation.fromJson(json["7d"] ?? {"": ""}),
    the30D: DateInformation.fromJson(json["30d"] ?? {"": ""}),
    the365D: DateInformation.fromJson(json["365d"] ?? {"": ""}),
    ytd: DateInformation.fromJson(json["ytd"] ?? {"": ""}),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "currency": currency,
    "symbol": symbol,
    "name": name,
    "logo_url": logoUrl,
    "status": status,
    "price": price,
    "price_date": priceDate!.toIso8601String(),
    "price_timestamp": priceTimestamp!.toIso8601String(),
    "circulating_supply": circulatingSupply,
    "max_supply": maxSupply,
    "market_cap": marketCap,
    "market_cap_dominance": marketCapDominance,
    "num_exchanges": numExchanges,
    "num_pairs": numPairs,
    "num_pairs_unmapped": numPairsUnmapped,
    "first_candle": firstCandle,
    "first_trade": firstTrade,
    "first_order_book": firstOrderBook,
    "rank": rank,
    "rank_delta": rankDelta,
    "high": high,
    "high_timestamp": highTimestamp,
    "1d": the1D!.toJson(),
    "7d": the7D!.toJson(),
    "30d": the30D!.toJson(),
    "365d": the365D!.toJson(),
    "ytd": ytd!.toJson(),
  };
}