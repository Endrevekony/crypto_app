import 'dart:convert';

List<CryptoHistoryModel> cryptoHistoryModelFromJson(String str) => List<CryptoHistoryModel>.from(json.decode(str).map((x) => CryptoHistoryModel.fromJson(x)));

String cryptoHistoryModelToJson(List<CryptoHistoryModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CryptoHistoryModel {
  CryptoHistoryModel({
    required this.currency,
    required this.timestamps,
    required this.prices,
  });

  final String currency;
  final List<DateTime> timestamps;
  final List<String> prices;

  factory CryptoHistoryModel.fromJson(Map<String, dynamic> json) => CryptoHistoryModel(
    currency: json["currency"],
    timestamps: List<DateTime>.from(json["timestamps"].map((x) => DateTime.parse(x))),
    prices: List<String>.from(json["prices"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "currency": currency,
    "timestamps": List<dynamic>.from(timestamps.map((x) => x.toIso8601String())),
    "prices": List<dynamic>.from(prices.map((x) => x)),
  };
}
