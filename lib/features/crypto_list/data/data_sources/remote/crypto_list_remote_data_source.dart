import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:http/http.dart' as http;

abstract class CryptoListRemoteDataSource {
  Future<List<CryptoItemModel>> getAllCryptos({required int page});
  Future<List<CryptoItemModel>> search({required String keyword});

}

class CryptoListRemoteDataSourceImpl implements CryptoListRemoteDataSource {
  final http.Client client;

  CryptoListRemoteDataSourceImpl({required this.client});

  @override
  Future<List<CryptoItemModel>> getAllCryptos({required int page}) async {
    final serverUrl = KUrls().getAllItems(page: page);
    final response = await client.get(
      Uri.parse(serverUrl),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
        final cryptoList = cryptoItemModelFromJson(response.body);
        return cryptoList;
    } if(response.statusCode == 429) {
      return throw TooMuchRequestException();
    } else {
      return throw ServerException();
    }
  }

  @override
  Future<List<CryptoItemModel>> search({required String keyword}) async {
    final serverUrl = KUrls().searchCryptos(keyword: keyword);
    final response = await client.get(
      Uri.parse(serverUrl),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      final cryptoList = cryptoItemModelFromJson(response.body);
      return cryptoList;
    } if(response.statusCode == 429) {
      return throw TooMuchRequestException();
    } else {
      return throw ServerException();
    }
  }
}
