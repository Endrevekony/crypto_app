import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:http/http.dart' as http;

abstract class CryptoHistoryRemoteDataSource {
  Future<List<CryptoHistoryModel>> getHistory({required String cryptoSymbol, required DateTime startDate, required bool isUsd});
}

class CryptoHistoryRemoteDataSourceImpl implements CryptoHistoryRemoteDataSource {
  final http.Client client;

  CryptoHistoryRemoteDataSourceImpl({required this.client});

  @override
  Future<List<CryptoHistoryModel>> getHistory({required String cryptoSymbol, required DateTime startDate, required bool isUsd}) async {
    final dateRfc = startDate.toUtc().toIso8601String();
    final String eurOrUsd = (isUsd) ? "USD" : "EUR";
    final serverUrl = KUrls().getHistoryUrl(cryptoSymbol: cryptoSymbol, startDate: dateRfc, isUsd: eurOrUsd);
    final response = await client.get(
      Uri.parse(serverUrl),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      final cryptoList = cryptoHistoryModelFromJson(response.body);
      return cryptoList;
    } if(response.statusCode == 429) {
      return throw TooMuchRequestException();
    } else {
      return throw ServerException();
    }
  }
}
