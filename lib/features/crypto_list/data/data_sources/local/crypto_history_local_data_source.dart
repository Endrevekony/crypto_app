import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class CryptoHistoryLocalDataSource {
  Future<List<CryptoHistoryModel>> getLastHistory({required String cryptoId});

  Future<void> cacheHistory(List<CryptoHistoryModel> model);
}

class CryptoHistoryLocalDataSourceImpl implements CryptoHistoryLocalDataSource {
  final SharedPreferences sharedPreferences;

  CryptoHistoryLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<CryptoHistoryModel>> getLastHistory({required String cryptoId}) {
    final jsonString = sharedPreferences.getString("CACHED_${cryptoId}_HISTORY");
    if (jsonString != null) {
      return Future.value(cryptoHistoryModelFromJson(jsonString));
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheHistory(List<CryptoHistoryModel> model) {
    return sharedPreferences.setString(
      "CACHED_${model.first.currency}_HISTORY",
      cryptoHistoryModelToJson(model),
    );
  }
}
