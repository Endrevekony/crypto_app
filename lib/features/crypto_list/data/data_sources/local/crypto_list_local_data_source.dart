import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class CryptoListLocalDataSource {
  Future<List<CryptoItemModel>> getLastCryptos();

  Future<void> cacheCryptos(List<CryptoItemModel> model);
}

const cachedCryptoItems = 'CACHED_CRYPTO_ITEMS';

class CryptoListLocalDataSourceImpl implements CryptoListLocalDataSource {
  final SharedPreferences sharedPreferences;

  CryptoListLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<CryptoItemModel>> getLastCryptos() {
    final jsonString = sharedPreferences.getString(cachedCryptoItems);
    if (jsonString != null) {
      return Future.value(cryptoItemModelFromJson(jsonString));
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheCryptos(List<CryptoItemModel> model) {
    return sharedPreferences.setString(
        cachedCryptoItems, cryptoItemModelToJson(model));
  }
}
