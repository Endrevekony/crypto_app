import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/network_info.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/local/crypto_history_local_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/local/crypto_list_local_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/remote/crypto_history_remote_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/data_sources/remote/crypto_list_remote_data_source.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:crypto_app/features/crypto_list/domain/repositories/crypto_list_repository.dart';
import 'package:dfunc/dfunc.dart';

class CryptoListRepositoryImpl implements CryptoListRepository {
  final CryptoListRemoteDataSource remoteDataSource;
  final CryptoListLocalDataSource localDataSource;
  final CryptoHistoryLocalDataSource historyLocalDataSource;
  final CryptoHistoryRemoteDataSource historyRemoteDataSource;

  final NetworkInfo networkInfo;

  CryptoListRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
    required this.historyLocalDataSource,
    required this.historyRemoteDataSource,
  });

  @override
  Future<Either<Failure, List<CryptoItemModel>>> getAllCryptos({required int page}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getAllCryptos(page: page);
        localDataSource.cacheCryptos(result);
        return Either.right(result);
      } on ServerException {
        return Either.left(ServerFailure());
      } on TooMuchRequestException {
        return Either.left(TooMuchRequestFailure());
      }
    } else {
      try {
        final localResult = await localDataSource.getLastCryptos();
        return Either.right(localResult);
      } on CacheException {
        return Either.left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<CryptoHistoryModel>>> getHistory({
    required String cryptoName,
    required bool isUsd,
    required DateTime date,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await historyRemoteDataSource.getHistory(cryptoSymbol: cryptoName, startDate: date, isUsd: isUsd);
        if(result.isNotEmpty){
          historyLocalDataSource.cacheHistory(result);
        }
        return Either.right(result);
      } on ServerException {
        return Either.left(ServerFailure());
      } on TooMuchRequestException {
        return Either.left(TooMuchRequestFailure());
      }
    } else {
      try {
        final localResult = await historyLocalDataSource.getLastHistory(cryptoId: cryptoName);
        return Either.right(localResult);
      } on CacheException {
        return Either.left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<CryptoItemModel>>> search({required String keyWord}) async{
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.search(keyword: keyWord.toUpperCase());
        return Either.right(result);
      } on TooMuchRequestException {
        return Either.left(TooMuchRequestFailure());
      } on ServerException {
        return Either.left(ServerFailure());
      }
    } else {
      try {
        final localResult = await localDataSource.getLastCryptos();
        for(CryptoItemModel item in localResult){
          if(item.name == keyWord){
            List<CryptoItemModel> list = [];
            list.add(item);
            return Either.right(list);
          }
        }
        return Either.left(CacheFailure());
      } on CacheException {
        return Either.left(CacheFailure());
      }
    }
  }
}
