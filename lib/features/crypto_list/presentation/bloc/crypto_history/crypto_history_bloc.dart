import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:crypto_app/features/crypto_list/domain/use_cases/get_history_use_case.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'crypto_history_event.dart';
part 'crypto_history_state.dart';

class CryptoHistoryBloc extends Bloc<CryptoHistoryEvent, CryptoHistoryState> {
  final GetHistoryUseCase getHistory;
  CryptoHistoryBloc({required this.getHistory}) : super(NoHistory());

  @override
  Stream<CryptoHistoryState> mapEventToState(
      CryptoHistoryEvent event,
      ) async* {
    if (event is GetHistory) {
      yield LoadingHistory();
      final failureOrCryptoHistory = await getHistory(CryptoHistoryParams(cryptoName: event.cryptoSymbol, isUsd: event.isUsd, date: event.dateTime));
      yield* _eitherCryptoListOrErrorState(failureOrCryptoHistory);
    }
  }

  Stream<CryptoHistoryState> _eitherCryptoListOrErrorState(
      Either<Failure, List<CryptoHistoryModel>> either,
      ) async* {
    yield either.fold(
          (failure) => HistoryError(message: _mapFailureToMessage(failure)),
          (cryptoList) => LoadedHistory(cryptoHistory: cryptoList),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return KStrings.serverException.tr();
      case CacheFailure:
        return KStrings.cacheException.tr();
      case TooMuchRequestFailure:
        return KStrings.tooMuchRequestException.tr();
      default:
        return KStrings.unexpectedException.tr();
    }
  }
}
