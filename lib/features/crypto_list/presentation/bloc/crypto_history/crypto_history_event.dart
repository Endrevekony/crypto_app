part of 'crypto_history_bloc.dart';

abstract class CryptoHistoryEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetHistory extends CryptoHistoryEvent {
  final String cryptoSymbol;
  final bool isUsd;
  final DateTime dateTime;

  GetHistory({required this.isUsd, required this.cryptoSymbol, required this.dateTime});

  @override
  List<Object> get props => [isUsd, cryptoSymbol, dateTime];
}