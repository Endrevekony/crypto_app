part of 'crypto_history_bloc.dart';

abstract class CryptoHistoryState extends Equatable {
  @override
  List<Object> get props => [];
}

class NoHistory extends CryptoHistoryState {}

class LoadingHistory extends CryptoHistoryState {}

class LoadedHistory extends CryptoHistoryState {
  final List<CryptoHistoryModel> cryptoHistory;

  LoadedHistory({required this.cryptoHistory});

  @override
  List<Object> get props => [cryptoHistory];
}

class HistoryError extends CryptoHistoryState {
  final String message;

  HistoryError({required this.message});

  @override
  List<Object> get props => [message];
}
