part of 'crypto_list_bloc.dart';

abstract class CryptoListState extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyCryptoList extends CryptoListState {}

class LoadingCryptoList extends CryptoListState {}

class LoadedCryptoList extends CryptoListState {
  final List<CryptoItemModel> cryptoList;

  LoadedCryptoList({required this.cryptoList});

  @override
  List<Object> get props => [cryptoList];
}

class LoadedSearch extends CryptoListState {
  final List<CryptoItemModel> crypto;

  LoadedSearch({required this.crypto});

  @override
  List<Object> get props => [crypto];
}

class CryptoListError extends CryptoListState {
  final String message;

  CryptoListError({required this.message});

  @override
  List<Object> get props => [message];
}
