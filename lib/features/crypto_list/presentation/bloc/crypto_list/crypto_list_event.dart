part of 'crypto_list_bloc.dart';

abstract class CryptoListEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetCryptoList extends CryptoListEvent {
  final int page;

  GetCryptoList({required this.page});

  @override
  List<Object> get props => [page];
}

class SearchCrypto extends CryptoListEvent {
  final String search;

  SearchCrypto({required this.search});

  @override
  List<Object> get props => [search];
}
