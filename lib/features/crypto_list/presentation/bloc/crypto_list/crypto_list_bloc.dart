import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/domain/use_cases/get_crypto_list_use_case.dart';
import 'package:crypto_app/features/crypto_list/domain/use_cases/search_crypto.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'crypto_list_event.dart';
part 'crypto_list_state.dart';

class CryptoListBloc extends Bloc<CryptoListEvent, CryptoListState> {
  final GetCrypoListUseCase getCryptoList;
  final SearchCryptoUseCase searchCrypto;
  CryptoListBloc({required this.getCryptoList, required this.searchCrypto})
      : super(EmptyCryptoList());

  @override
  Stream<CryptoListState> mapEventToState(
    CryptoListEvent event,
  ) async* {
    if (event is GetCryptoList) {
      if (event.page == 1) {
        yield LoadingCryptoList();
      }
      final failureOrCryptoList = await getCryptoList(CryptoListParams(
        page: event.page,
      ));
      yield* _eitherCryptoListOrErrorState(failureOrCryptoList);
    }
    if (event is SearchCrypto) {
      final failureOrCrypto = await searchCrypto(SearchParams(
        keyword: event.search,
      ));
      yield* _eitherCryptoResultOrErrorState(failureOrCrypto);
    }
  }

  Stream<CryptoListState> _eitherCryptoListOrErrorState(
    Either<Failure, List<CryptoItemModel>> either,
  ) async* {
    yield either.fold(
      (failure) => CryptoListError(message: _mapFailureToMessage(failure)),
      (cryptoList) => LoadedCryptoList(cryptoList: cryptoList),
    );
  }

  Stream<CryptoListState> _eitherCryptoResultOrErrorState(
    Either<Failure, List<CryptoItemModel>> either,
  ) async* {
    yield either.fold(
      (failure) => CryptoListError(message: _mapFailureToMessage(failure)),
      (cryptoList) => LoadedSearch(crypto: cryptoList),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return KStrings.serverException.tr();
      case CacheFailure:
        return KStrings.cacheException.tr();
      case TooMuchRequestFailure:
        return KStrings.tooMuchRequestException.tr();
      default:
        return KStrings.unexpectedException.tr();
    }
  }
}
