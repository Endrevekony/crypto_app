import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_history/crypto_history_bloc.dart';
import 'package:crypto_app/features/crypto_list/presentation/pages/crypto_item_page.dart';
import 'package:crypto_app/features/favorites/presentation/widgets/favorites_star_widget.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../injection_container.dart';

class ListItemWidget extends StatelessWidget {
  final CryptoItemModel model;
  final bool isSlidable;
  const ListItemWidget({required this.model, required this.isSlidable});
  static SlidableController controller = SlidableController();
  @override
  Widget build(BuildContext context) {
    return (isSlidable) ? Slidable(
      controller: controller,
      actionPane: const SlidableDrawerActionPane(),
      actions: <Widget>[
        Container(
          color: KColors.darkPurple,
          child: FavoritesStarWidget(
            model: model,
          ),
        ),
      ],
      child: listItemWidget(context: context),
    ) : listItemWidget(context: context);
  }

  Widget checkIfSvgOrPng({required String url}) {
    if (url.endsWith(".svg")) {
      return networkSvg(url: url);
    } else {
      return CachedNetworkImage(
        imageUrl: url,
      );
    }
  }

  Widget networkSvg({required String url}) {
    return SvgPicture.network(
      url,
      placeholderBuilder: (BuildContext context) =>
          const CircularProgressIndicator.adaptive(),
    );
  }

  Widget listItemWidget({required BuildContext context}) {
    return ListTile(
      title: Text(model.name),
      leading: SizedBox(
        height: 30,
        width: 30,
        child: checkIfSvgOrPng(url: model.logoUrl),
      ),
      trailing: Text('${model.price} USD'),
      onTap: () {
        final currentDate = DateTime.now();
        final date = DateTime(
            currentDate.year, currentDate.month, currentDate.day - 1);
        sl<CryptoHistoryBloc>().add(
          GetHistory(isUsd: true, cryptoSymbol: model.symbol, dateTime: date),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => CryptoItemPage(model: model),
          ),
        );
      },
    );
  }
}
