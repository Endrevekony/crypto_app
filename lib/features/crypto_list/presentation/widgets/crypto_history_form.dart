import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/widgets/error_widget.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:crypto_app/features/crypto_list/domain/entities/date_information.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_history/crypto_history_bloc.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'chart_widget.dart';

import '../../../../injection_container.dart';

class CryptoHistoryForm extends StatefulWidget {
  final CryptoHistoryModel? history;
  final CryptoItemModel itemModel;

  const CryptoHistoryForm({required this.history, required this.itemModel});

  @override
  _CryptoHistoryFormState createState() => _CryptoHistoryFormState();
}

class _CryptoHistoryFormState extends State<CryptoHistoryForm> {
  static DateTime currentDate = DateTime.now();
  static DateTime savedDate = DateTime(
    currentDate.year,
    currentDate.month,
    currentDate.day - 1,
  );
  static int savedButton = 1;
  static bool isUsd = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: ListView(
        shrinkWrap: true,
        children: [
          (widget.history) != null
              ? ChartWidget(
                  model: widget.history,
                )
              : AppErrorWidget(errorMessage: KStrings.serverException.tr()),
          (widget.itemModel.highTimestamp != "") ? Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  changeDateButton(
                    text: '1h',
                    date: DateTime(
                      currentDate.year,
                      currentDate.month,
                      currentDate.day,
                      currentDate.hour - 1,
                    ),
                    position: 0,
                  ),
                  changeDateButton(
                    text: '1d',
                    date: DateTime(
                      currentDate.year,
                      currentDate.month,
                      currentDate.day,
                      currentDate.hour - 24,
                    ),
                    position: 1,
                  ),
                  changeDateButton(
                    text: '7d',
                    date: DateTime(
                      currentDate.year,
                      currentDate.month,
                      currentDate.day - 7,
                    ),
                    position: 2,
                  ),
                  changeDateButton(
                    text: '1m',
                    date: DateTime(
                      currentDate.year,
                      currentDate.month - 1,
                      currentDate.day,
                    ),
                    position: 3,
                  ),
                  changeDateButton(
                    text: '1y',
                    date: DateTime(
                      currentDate.year - 1,
                      currentDate.month,
                      currentDate.day,
                    ),
                    position: 4,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: RadioListTile(
                      activeColor: KColors.darkPurple,
                      value: true,
                      groupValue: isUsd,
                      onChanged: (_) => changeCurrency(currency: true),
                      title: const Text("USD"),
                    ),
                  ),
                  Expanded(
                    child: RadioListTile(
                      activeColor: KColors.darkPurple,
                      value: false,
                      groupValue: isUsd,
                      onChanged: (_) => changeCurrency(currency: false),
                      title: const Text("EUR"),
                    ),
                  ),
                ],
              ),
              datatTile(),
            ],
          ) : Container(),
        ],
      ),
    );
  }

  Widget changeDateButton(
      {required String text, required DateTime date, required int position}) {
    return GestureDetector(
      onTap: () {
        savedButton = position;
        savedDate = date;
        Future.delayed(const Duration(milliseconds: 200), () {
          sl<CryptoHistoryBloc>().add(
            GetHistory(
                isUsd: isUsd,
                cryptoSymbol: widget.history!.currency,
                dateTime: date),
          );
        });
      },
      child: Card(
        color: (position == savedButton) ? KColors.lightPurple : Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Text(
            text,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: KColors.darkPurple, width: 2),
          borderRadius: BorderRadius.circular(10),
        ),
        shadowColor: KColors.darkPurple,
      ),
    );
  }

  void changeCurrency({required bool currency}) {
    setState(() {
      isUsd = currency;
    });
    Future.delayed(const Duration(milliseconds: 500), () {
      sl<CryptoHistoryBloc>().add(
        GetHistory(
          isUsd: isUsd,
          cryptoSymbol: widget.history!.currency,
          dateTime: savedDate,
        ),
      );
    });
  }

  Widget datatTile() {
    DateInformation? date;
    if (savedButton == 0 || savedButton == 1) {
      date = widget.itemModel.the1D;
    } else if (savedButton == 2) {
      date = widget.itemModel.the7D;
    } else if (savedButton == 3) {
      date = widget.itemModel.the30D;
    } else if (savedButton == 4) {
      date = widget.itemModel.the365D;
    }
    if (date != null) {
      return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "volume".tr() + ":",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  width: 3,
                ),
                Text(
                  date.volume + " USD",
                  style: const TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Text(
                  "marketCapChange".tr() + ":",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  width: 3,
                ),
                Text(
                  date.marketCapChange + " USD",
                  style: const TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Text(
                  "priceChange".tr() + ":",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  width: 3,
                ),
                Text(
                  date.priceChange + " USD",
                  style: const TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      return AppErrorWidget(
        errorMessage: KStrings.noDataFound.tr(),
      );
    }
  }
}
