import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class ChartWidget extends StatelessWidget {
  final CryptoHistoryModel? model;

  const ChartWidget({required this.model});

  static bool maxXshowed = false;
  @override
  Widget build(BuildContext context) {
    final minY = getYMinValue();
    final maxY = getYMaxValue();
    final minX = model!.timestamps.first.millisecondsSinceEpoch;
    return Center(
      child: SizedBox(
        height: (MediaQuery.of(context).orientation == Orientation.landscape) ? MediaQuery.of(context).size.height / 1.2 : MediaQuery.of(context).size.height / 2.2,
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: LineChart(
            LineChartData(
              lineBarsData: [
                LineChartBarData(
                  spots: getValues(),
                  isCurved: true,
                  show: true,
                  barWidth: 2,
                  colors: [
                    KColors.darkPurple,
                  ],
                ),
              ],
              titlesData: FlTitlesData(
                show: true,
                bottomTitles: SideTitles(
                    showTitles: true,
                    margin: 20,
                    getTitles: (value) {
                      final valueDate = DateTime.fromMillisecondsSinceEpoch(
                        value.toInt(),
                      );
                      if (value == minX) {
                        maxXshowed = false;
                        return valueDate.toString().substring(0, 10);
                      }
                      if (!maxXshowed) {
                        if (DateTime.now().month == valueDate.month) {
                          if (DateTime.fromMillisecondsSinceEpoch(minX)
                                  .difference(valueDate)
                                  .inDays
                                  .abs() >
                              25) {
                            maxXshowed = true;
                            return valueDate.toString().substring(0, 10);
                          }
                        }
                      }
                      return '';
                    }),
                leftTitles: SideTitles(
                    showTitles: true,
                    margin: 20,
                    getTitles: (value) {
                      if (value.toInt() == maxY.toInt()) {
                        return maxY.toInt().toString();
                      }
                      if (value == minY) {
                        return minY.toInt().toString();
                      }
                      return '';
                    }),
              ),
              minY: minY,
              maxY: maxY + (maxY / 100),
              minX: minX.toDouble(),
            ),
          ),
        ),
      ),
    );
  }

  List<FlSpot> getValues() {
    final List<FlSpot> values = [];
    for (String item in model!.prices) {
      int index = model!.prices.indexOf(item);
      values.add(
        FlSpot(model!.timestamps[index].millisecondsSinceEpoch.toDouble(),
            double.parse(item)),
      );
    }
    return values;
  }

  double getYMaxValue() {
    double highest = double.parse(model!.prices.first);
    for (String item in model!.prices) {
      if (double.parse(item) > highest) {
        highest = double.parse(item);
      }
    }
    return highest;
  }

  double getYMinValue() {
    double lowest = double.parse(model!.prices.first);
    for (String item in model!.prices) {
      if (double.parse(item) < lowest) {
        lowest = double.parse(item);
      }
    }
    return lowest;
  }
}
