import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_list/crypto_list_bloc.dart';
import 'package:crypto_app/features/crypto_list/presentation/widgets/list_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:outline_search_bar/outline_search_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../injection_container.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  static List<CryptoItemModel> listFromApi = List.empty(growable: true);
  static int pageNumber = 1;
  static CryptoListState currentState = EmptyCryptoList();
  final TextEditingController _searchController = TextEditingController();
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 700), () {
      sl<CryptoListBloc>().add(GetCryptoList(page: 1));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isSearched = false;
    RefreshController _refreshController =
        RefreshController(initialRefresh: false);
    return SmartRefresher(
      controller: _refreshController,
      header: const MaterialClassicHeader(),
      onRefresh: () {
        listFromApi.clear();
        sl<CryptoListBloc>().add(GetCryptoList(page: 1));
        setState(() {
          _searchController.clear();
          _refreshController.refreshCompleted();
        });
      },
      child: ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        controller: _scrollController
          ..addListener(() {
            if (_scrollController.offset ==
                    _scrollController.position.maxScrollExtent &&
                currentState != LoadingCryptoList()) {
              Future.delayed(const Duration(milliseconds: 500), (){
                  sl<CryptoListBloc>().add(GetCryptoList(page: ++pageNumber));
              });
            }
          }),
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: SizedBox(
              height: 100,
              width: 100,
              child: Image.asset('lib/core/assets/images/appIcon.png'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: OutlineSearchBar(
                      textEditingController: _searchController,
                      hintText: KStrings.search.tr(),
                      onKeywordChanged: (keyword) {
                        if(keyword == ""){
                          sl<CryptoListBloc>().add(GetCryptoList(page: 1));
                        }
                      },
                      onSearchButtonPressed: (searchKeyword) {
                        setState(() {
                          isSearched = true;
                        });
                        sl<CryptoListBloc>().add(SearchCrypto(search: searchKeyword));
                      },
                      onClearButtonPressed: (value) {
                        if(value != ""){
                          if(isSearched) {
                            sl<CryptoListBloc>().add(GetCryptoList(page: 1));
                            isSearched = false;
                          }
                        }
                      },
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(FontAwesomeIcons.sortAlphaDown,
                      color: KColors.darkPurple),
                  onPressed: () {
                    listFromApi.sort((a, b) => a.name.compareTo(b.name));
                    setState(() {
                      _refreshController.refreshCompleted();
                    });
                  },
                ),
                IconButton(
                  icon: const Icon(FontAwesomeIcons.list,
                      color: KColors.darkPurple),
                  onPressed: () {
                    listFromApi.sort((a, b) => a.price.compareTo(b.price));
                    setState(() {
                      _refreshController.refreshCompleted();
                    });
                  },
                ),
                IconButton(
                  icon: const Icon(
                    FontAwesomeIcons.medal,
                    color: KColors.darkPurple,
                  ),
                  onPressed: () {
                    listFromApi.sort((a, b) => a.rank.compareTo(b.rank));
                    setState(() {
                      _refreshController.refreshCompleted();
                    });
                  },
                ),
              ],
            ),
          ),
          BlocConsumer<CryptoListBloc, CryptoListState>(
            bloc: sl<CryptoListBloc>(),
            listener: (context, state) {
              if (state is CryptoListError) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                      backgroundColor: KColors.errorRed,
                      duration: const Duration(seconds: 1),
                      content: Text(state.message)),
                );
                _refreshController.refreshCompleted();
              }
            },
            builder: (context, state) {
              currentState = state;
              if (state is EmptyCryptoList) {
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              }
              if (state is LoadingCryptoList) {
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              }
              if (state is LoadedCryptoList) {
                listFromApi.addAll(state.cryptoList);
                final ids = listFromApi.map((e) => e.id).toSet();
                listFromApi.retainWhere((x) => ids.remove(x.id));
                return cryptoListView(itemList: listFromApi);
              }
              if (state is LoadedSearch) {
                if(state.crypto.isEmpty){
                  return ListTile(
                    title: Center(
                      child: Text(
                        KStrings.noSearchResults.tr(),
                      ),
                    ),
                  );
                }
                listFromApi.add(state.crypto.first);
                return cryptoListView(itemList: state.crypto);
              }
              return cryptoListView(itemList: listFromApi);
            },
          )
        ],
      ),
    );
  }

  Widget cryptoListView({required List<CryptoItemModel> itemList}) {
      return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: itemList.length,
        itemBuilder: (context, index) {
          return ListItemWidget(model: itemList[index], isSlidable: true,);
        },
      );
    }
}
