import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/widgets/error_widget.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_history_model.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_history/crypto_history_bloc.dart';
import 'package:crypto_app/features/crypto_list/presentation/widgets/crypto_history_form.dart';
import 'package:crypto_app/features/favorites/presentation/bloc/favorites_bloc.dart';
import 'package:crypto_app/features/favorites/presentation/widgets/favorites_star_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class CryptoItemPage extends StatelessWidget {
  final CryptoItemModel model;
  static CryptoHistoryModel? history;
  const CryptoItemPage({required this.model});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        sl<FavoritesBloc>().add(GetFavorites());
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            model.name,
            style: const TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.keyboard_backspace_sharp,
              color: Colors.white,
            ),
            onPressed: () {
              sl<FavoritesBloc>().add(GetFavorites());
              Navigator.pop(context);
            },
          ),
          actions: [
            FavoritesStarWidget(
              model: model,
            ),
          ],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                BlocConsumer<CryptoHistoryBloc, CryptoHistoryState>(
                    listener: (context, state) {
                      if (state is HistoryError) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                              backgroundColor: KColors.errorRed,
                              duration: const Duration(seconds: 1),
                              content: Text(state.message)),
                        );
                      }
                    },
                    bloc: sl<CryptoHistoryBloc>(),
                    builder: (context, state) {
                      if (state is NoHistory) {
                        final currentDate = DateTime.now();
                        final date = DateTime(
                          currentDate.year,
                          currentDate.month,
                          currentDate.day - 1,
                        );
                        sl<CryptoHistoryBloc>().add(
                          GetHistory(
                            isUsd: true,
                            cryptoSymbol: model.symbol,
                            dateTime: date,
                          ),
                        );
                      }
                      if (state is LoadingHistory) {
                        return const Center(
                          child: CircularProgressIndicator.adaptive(),
                        );
                      }
                      if (state is LoadedHistory) {
                        if (state.cryptoHistory.isEmpty) {
                          return AppErrorWidget(
                              errorMessage: KStrings.noHistory.tr());
                        }
                        history = state.cryptoHistory.first;
                        return CryptoHistoryForm(
                          history: state.cryptoHistory.first,
                          itemModel: model,
                        );
                      }
                      return CryptoHistoryForm(
                          itemModel: model, history: history);
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
