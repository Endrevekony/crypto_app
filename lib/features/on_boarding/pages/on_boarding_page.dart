import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/on_boarding/bloc/on_boarding_bloc.dart';
import 'package:crypto_app/injection_container.dart';
import 'package:crypto_app/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'on_boarding_mini_pages.dart';
import 'package:easy_localization/easy_localization.dart';


class OnBoarding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: IntroductionScreen(
        globalBackgroundColor: KColors.appPurple,
        showNextButton: true,
        next: const Icon(
          FontAwesomeIcons.arrowRight,
          color: Colors.white,
        ),
        showSkipButton: true,
        skip: Text(
          KStrings.skip.tr(),
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold
          ),
        ),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          activeSize: Size(22.0, 10.0),
          color: Colors.white,
          activeColor: KColors.darkPurple,
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25),
            ),
          ),
        ),
        onDone: () => _onIntroEnd(context),
        done: const CircleAvatar(
          radius: 25,
          backgroundColor: KColors.darkPurple,
          child: Icon(
            FontAwesomeIcons.arrowRight,
            color: Colors.white,
          ),
        ),
        pages: onBoardingPages(
          context: context,
        ),
      ),
    );
  }

  void _onIntroEnd(context) {
    sl<OnBoardingBloc>().add(OnBoardingDidFinish());
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (_) => CryptoApp(),
      ),
    );
  }
}
