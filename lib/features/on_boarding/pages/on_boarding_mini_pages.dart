import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

List<PageViewModel> onBoardingPages({required BuildContext context}) {
  const bodyStyle = TextStyle(
    fontSize: 19.0,
    color: Colors.white,
  );
  const pageDecoration = PageDecoration(
    titleTextStyle: TextStyle(
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
      color: Colors.white,
    ),
    bodyTextStyle: bodyStyle,
    descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
    pageColor: KColors.appPurple,
    imagePadding: EdgeInsets.zero,
  );
  return [
    PageViewModel(
      title: KStrings.onBoardingTitle1.tr(),
      body: KStrings.onBoardingDescription1.tr(),
      image: _buildImage(assetName: 'onBoarding1', context: context),
      decoration: pageDecoration,
    ),
    PageViewModel(
      title: KStrings.onBoardingTitle2.tr(),
      body: KStrings.onBoardingDescription2.tr(),
      image: _buildImage(assetName: 'onBoarding2', context: context),
      decoration: pageDecoration,
    ),
    PageViewModel(
      title: KStrings.onBoardingTitle3.tr(),
      body: KStrings.onBoardingDescription3.tr(),
      image: _buildImage(assetName: 'onBoarding3', context: context),
      decoration: pageDecoration,
    ),
  ];
}

Widget _buildImage({required String assetName, required BuildContext context}) {
  return Align(
    child: Padding(
      padding: const EdgeInsets.all(20),
      child: Image.asset(
        'lib/core/assets/images/$assetName.png',
        width: MediaQuery.of(context).size.width / 2,
      ),
    ),
    alignment: Alignment.bottomCenter,
  );
}
