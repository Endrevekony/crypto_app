part of 'on_boarding_bloc.dart';

abstract class OnBoardingState extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyOnBoarding extends OnBoardingState {}

class ShouldShowOnBoarding extends OnBoardingState {}

class ShouldShowApp extends OnBoardingState {}
