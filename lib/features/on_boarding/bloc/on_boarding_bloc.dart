import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'on_boarding_event.dart';
part 'on_boarding_state.dart';

class OnBoardingBloc extends Bloc<OnBoardingEvent, OnBoardingState> {
  OnBoardingBloc() : super(EmptyOnBoarding());

  @override
  Stream<OnBoardingState> mapEventToState(
      OnBoardingEvent event,
      ) async* {
    const String firstTimeKey = 'FIRST_TIME';
    if (event is CheckIfShouldShowOnBoarding) {
      SharedPreferences sp = await SharedPreferences.getInstance();
      if (sp.getBool(firstTimeKey) == null) {
        yield ShouldShowOnBoarding();
      } else {
        yield ShouldShowApp();
      }
    }
    if (event is OnBoardingDidFinish) {
      SharedPreferences sp = await SharedPreferences.getInstance();
      await sp.setBool(firstTimeKey, true);
      yield ShouldShowApp();
    }
  }
}
