part of 'on_boarding_bloc.dart';

abstract class OnBoardingEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CheckIfShouldShowOnBoarding extends OnBoardingEvent {}

class OnBoardingDidFinish extends OnBoardingEvent {}
