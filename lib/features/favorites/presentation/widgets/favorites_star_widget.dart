import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/favorites/presentation/bloc/favorites_bloc.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../injection_container.dart';

class FavoritesStarWidget extends StatefulWidget {
  final CryptoItemModel model;

  const FavoritesStarWidget({required this.model});

  @override
  _FavoritesStarWidgetState createState() => _FavoritesStarWidgetState();
}

class _FavoritesStarWidgetState extends State<FavoritesStarWidget> {
  bool isFavorite = false;
  bool isLoading = false;

  @override
  void initState() {
    sl<FavoritesBloc>().add(GetFavorites());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BlocListener<FavoritesBloc, FavoritesState>(
      bloc: sl<FavoritesBloc>(),
      listener: (context, state) {
        if (state is ChangedFavorite) {
          isLoading = false;
          isFavorite = state.isFavorite;
          setState(() {});
        }
        if (state is LoadedFavorites) {
          if(state.favorites.contains(widget.model.symbol.toLowerCase())){
            isFavorite = true;
          }
          setState(() {});
        }
        if (state is LoadingStarWidget) {
          isLoading = true;
          setState(() {});
        }
        if (state is FavoritesError) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(state.message)),
          );
          setState(() {});
        }
      },
      child: IconButton(
        onPressed: () {
          sl<FavoritesBloc>().add(
            ChangeFavoriteStar(
              cryptoSymbol: widget.model.symbol.toLowerCase(),
              isFavorite: isFavorite,
            ),
          );
        },
        icon: (isLoading)
            ? const Center(
              child: SizedBox(
                height: 30,
                width: 30,
                child: CircularProgressIndicator.adaptive(
                    backgroundColor: Colors.white,
                  ),
              ),
            )
            : Icon(
                (!isFavorite)
                    ? FontAwesomeIcons.star
                    : FontAwesomeIcons.solidStar,
              ),
        color: Colors.white,
      ),
    );
  }
}
