import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/widgets/error_widget.dart';
import 'package:crypto_app/features/crypto_list/data/models/crypto_data_model.dart';
import 'package:crypto_app/features/crypto_list/presentation/bloc/crypto_list/crypto_list_bloc.dart';
import 'package:crypto_app/features/crypto_list/presentation/widgets/list_item_widget.dart';
import 'package:crypto_app/features/favorites/presentation/bloc/favorites_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class FavoritesPage extends StatefulWidget {
  final String userId;
  const FavoritesPage({required this.userId});

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  static List<CryptoItemModel> cryptolist = [];
  static bool isLoading = true;

  @override
  void initState() {
    isLoading = true;
    cryptolist.clear();
    sl<FavoritesBloc>().add(GetFavorites());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: BlocListener<CryptoListBloc, CryptoListState>(
        bloc: sl<CryptoListBloc>(),
        listener: (context, state) {
          if (state is LoadedSearch) {
            cryptolist.clear();
            if (state.crypto.isNotEmpty) {
              cryptolist.addAll(state.crypto);
              setState(() {
                isLoading = false;
              });
            }
          }
        },
        child: BlocBuilder<FavoritesBloc, FavoritesState>(
          bloc: sl<FavoritesBloc>(),
          builder: (context, state) {
            if (state is ChangedFavorite) {
              sl<FavoritesBloc>().add(GetFavorites());
            }
            if (state is LoadingFavorites) {
              return const Center(child: CircularProgressIndicator.adaptive());
            }
            if (state is LoadingFavorites) {
              return const Center(child: CircularProgressIndicator.adaptive());
            }
            if (state is LoadedFavorites) {
              if (state.favorites.isEmpty) {
                return Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: Text(
                      KStrings.noFavoritesException.tr(),
                      style: const TextStyle(fontSize: 18),
                    ),
                  ),
                );
              } else {
                if(state.favorites.length != cryptolist.length){
                  getFavoriteCrypto(crypto: state.favorites);
                }
                return Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        KStrings.favorites.tr(),
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    (isLoading)
                        ? const Padding(
                            padding: EdgeInsets.all(10),
                            child: Center(
                              child: CircularProgressIndicator.adaptive(),
                            ),
                          )
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: state.favorites.length,
                            itemBuilder: (context, index) {
                              final String item = state.favorites[index];
                              if (cryptolist.isNotEmpty) {
                                return ListItemWidget(model: cryptolist[index], isSlidable: false,);
                              }
                              return Center(
                                child: Text(
                                    "${item.toUpperCase()} is not defined"),
                              );
                            },
                          ),
                  ],
                );
              }
            }
            if (state is FavoritesError) {
              return Center(
                child: AppErrorWidget(errorMessage: state.message),
              );
            }
            return const Center(child: CircularProgressIndicator.adaptive());
          },
        ),
      ),
    );
  }

  void getFavoriteCrypto({required List<String> crypto}) {
    String ids = '';
    for (String value in crypto.reversed) {
      ids = ids + ',' + value;
    }
    sl<CryptoListBloc>().add(SearchCrypto(search: ids));
  }
}
