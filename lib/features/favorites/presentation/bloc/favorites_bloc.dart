import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/add_to_favorites_use_case.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/get_favorites_use_case.dart';
import 'package:crypto_app/features/favorites/domain/use_cases/remove_from_favorites_use_case.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'favorites_events.dart';
part 'favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  final GetFavoritesUseCase getFavorites;
  final AddToFavoritesUseCase addToFavorites;
  final RemoveFromFavoritesUseCase removeFromFavorites;

  FavoritesBloc(
      {required this.getFavorites,
      required this.addToFavorites,
      required this.removeFromFavorites})
      : super(EmptyFavorites());

  @override
  Stream<FavoritesState> mapEventToState(
    FavoritesEvent event,
  ) async* {
    if (event is GetFavorites) {
      yield LoadingFavorites();
      final failureOrCryptos = await getFavorites(NoParams());
      yield* _eitherFavoritesOrErrorState(failureOrCryptos);
    }
    if (event is ChangeFavoriteStar) {
      yield LoadingStarWidget();
      if (!event.isFavorite) {
        final failureOrAddedCryptos = await addToFavorites(
          GetFavoritesParams(
            cryptoName: event.cryptoSymbol,
          ),
        );
        yield* _eitherAddedFavoriteOrErrorState(failureOrAddedCryptos);
      } else {
        final failureOrRemovedCryptos = await removeFromFavorites(
          RemoveFavoritesParams(
            cryptoId: event.cryptoSymbol,
          ),
        );
        yield* _eitherRemovedFavoriteOrErrorState(failureOrRemovedCryptos);
      }
    }
  }

  Stream<FavoritesState> _eitherFavoritesOrErrorState(
    Either<Failure, List<String>> either,
  ) async* {
    yield either.fold(
      (failure) => FavoritesError(message: _mapFailureToMessage(failure)),
      (favorites) => LoadedFavorites(favorites: favorites),
    );
  }

  Stream<FavoritesState> _eitherAddedFavoriteOrErrorState(
    Either<Failure, bool> either,
  ) async* {
    yield either.fold(
      (failure) => FavoritesError(message: _mapFailureToMessage(failure)),
      (favorite) => ChangedFavorite(isFavorite: true),
    );
  }

  Stream<FavoritesState> _eitherRemovedFavoriteOrErrorState(
      Either<Failure, bool> either,
      ) async* {
    yield either.fold(
          (failure) => FavoritesError(message: _mapFailureToMessage(failure)),
          (favorite) => ChangedFavorite(isFavorite: false),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return KStrings.serverException.tr();
      case CacheFailure:
        return KStrings.cacheException.tr();
      case NoFavoritesFailure:
        return KStrings.noFavoritesException.tr();
      default:
        return KStrings.unexpectedException.tr();
    }
  }
}
