part of 'favorites_bloc.dart';

abstract class FavoritesState extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyFavorites extends FavoritesState {}

class LoadingFavorites extends FavoritesState {}

class LoadingStarWidget extends FavoritesState {}

class LoadedFavorites extends FavoritesState {
  final List<String> favorites;

  LoadedFavorites({required this.favorites});

  @override
  List<Object> get props => [favorites];
}

class ChangedFavorite extends FavoritesState {
  final bool isFavorite;

  ChangedFavorite({required this.isFavorite});

  @override
  List<Object> get props => [isFavorite];
}


class FavoritesError extends FavoritesState {
  final String message;

  FavoritesError({required this.message});

  @override
  List<Object> get props => [message];
}
