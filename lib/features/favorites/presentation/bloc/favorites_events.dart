part of 'favorites_bloc.dart';

abstract class FavoritesEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetFavorites extends FavoritesEvent {}

class ChangeFavoriteStar extends FavoritesEvent {
  final String cryptoSymbol;
  final bool isFavorite;

  ChangeFavoriteStar({required this.cryptoSymbol, required this.isFavorite});

  @override
  List<Object> get props => [cryptoSymbol, isFavorite];
}