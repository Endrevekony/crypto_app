import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class FavoritesLocalDataSource {
  Future<List<String>> getLastFavorites();

  Future<void> cacheFavorites({required List<String> keys, required List<String> values});

  Future<void> addToFavorites({required String favoriteCryptoId, required String cryptoSymbol});

  Future<void> removeFromFavorites({required String favoriteCryptoId});

  Future<String?> getIdFromSymbol({required String symbol});


}

const cachedFavorites = 'CACHED_FAVORITES';

class FavoritesLocalDataSourceImpl implements FavoritesLocalDataSource {
  final SharedPreferences sharedPreferences;

  FavoritesLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<String>> getLastFavorites() {
    final jsonString = sharedPreferences.getStringList(cachedFavorites);
    if (jsonString != null) {
      return Future.value(jsonString);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheFavorites({required List<String> keys, required List<String> values}) async {
    int i = 0;
    for (String key in keys) {
        sharedPreferences.setString(
            key, values[i]);
        i++;
      }
    sharedPreferences.setStringList(
        cachedFavorites, keys);
  }

  @override
  Future<void> addToFavorites({required String favoriteCryptoId, required String cryptoSymbol}) async {
    try{
      final favorites = await getLastFavorites();
      await sharedPreferences.setString(favoriteCryptoId, cryptoSymbol);
      favorites.add(favoriteCryptoId);
      List<String> values = [];
      for(String item in favorites){
        final cachedString = sharedPreferences.getString(item);
        if(cachedString != null){
          values.add(cachedString);
        }
      }
      cacheFavorites(values: values, keys: favorites);
    } on CacheException {
      throw CacheException();
    }
  }

  @override
  Future<void> removeFromFavorites({required String favoriteCryptoId}) async {
    try{
      final favorites = await getLastFavorites();
      favorites.remove(favoriteCryptoId);
      await sharedPreferences.remove(favoriteCryptoId);
      List<String> values = [];
      for(String item in favorites){
        final cachedString = sharedPreferences.getString(item);
        if(cachedString != null){
          values.add(cachedString);
        }
      }
      cacheFavorites(values: values, keys: favorites);
    } on CacheException {
      throw CacheException();
    }
  }

  @override
  Future<String?> getIdFromSymbol({required String symbol}) async {
    final favorites = sharedPreferences.getStringList(cachedFavorites);
    if(favorites != null) {
      for (String item in favorites) {
        final cachedSymbol = sharedPreferences.getString(item);
        if(cachedSymbol == symbol){
          return item;
        }
      }
    } else {
      throw CacheException();
    }
  }
}
