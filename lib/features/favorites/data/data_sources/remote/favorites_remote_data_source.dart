import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

abstract class FavoritesRemoteDataSource {
  Future<Map<List<String>, List<String>>> getFavorites();
  Future<String> addToFavorites({required String cryptoSymbol});
  Future<bool> removeFromFavorites({required String cryptoSymbol});

}

class FavoritesRemoteDataSourceImpl implements FavoritesRemoteDataSource {
  final FirebaseDatabase firebaseDatabase;

  FavoritesRemoteDataSourceImpl({required this.firebaseDatabase});

  @override
  Future<Map<List<String>, List<String>>> getFavorites() async {
    try {
      final userId = FirebaseAuth.instance.currentUser!.uid;
      final snapshots = await getSnapshots(userId: userId);
      return { snapshots.keys.first : snapshots.values.first};
    } catch (e) {
      throw ServerFailure();
    }
  }

  Future<Map<List<String>,List<String>>> getSnapshots({required String userId}) async {
    final reference =
        firebaseDatabase.reference().child(userId).child("favorites");
    final data = await reference.once();
    if(data.value == null){
      return {[] : []};
    }
    List<String> ids = [];
    List<String> values = [];
    await reference.once().then(
      (DataSnapshot snapshot) {
        final mappedResults = Map<String, dynamic>.from(snapshot.value);
        mappedResults.forEach((key, value) {
          ids.add(key);
          values.add(value);
        },
        );
      });
    return { ids : values};
  }

  @override
  Future<String> addToFavorites({required String cryptoSymbol}) async {
    try {
      final userId = FirebaseAuth.instance.currentUser!.uid;
      final reference = firebaseDatabase.reference().child(userId).child("favorites");
      var order = reference.push();
      await order.set(cryptoSymbol);
      return Future.value(order.key);
    } catch (e) {
      throw ServerException();
    }
  }

  @override
  Future<bool> removeFromFavorites({required String cryptoSymbol}) async {
    try {
      final userId = FirebaseAuth.instance.currentUser!.uid;
      final reference = firebaseDatabase.reference().child(userId).child("favorites");
      await reference.child(cryptoSymbol).remove().catchError((_) {
        throw ServerException();
      });
      return Future.value(true);
    } catch (e) {
      throw ServerException();
    }
  }
}
