import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/network_info.dart';
import 'package:crypto_app/features/favorites/data/data_sources/local/favorites_local_data_source.dart';
import 'package:crypto_app/features/favorites/data/data_sources/remote/favorites_remote_data_source.dart';
import 'package:crypto_app/features/favorites/domain/repositories/favorites_repository.dart';
import 'package:dfunc/dfunc.dart';

class FavoritesRepositoryImpl implements FavoritesRepository {
  final FavoritesLocalDataSource localDataSource;
  final FavoritesRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  FavoritesRepositoryImpl({
    required this.networkInfo,
    required this.localDataSource,
    required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<String>>> getFavorites() async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getFavorites();
        if(result.keys.isNotEmpty){
          localDataSource.cacheFavorites(values: result.values.first, keys: result.keys.first);
          return Either.right(result.values.first);
        }
        return Either.left(NoFavoritesFailure());
      } on ServerException {
        return Either.left(ServerFailure());
      }
    } else {
      try {
        final localResult = await localDataSource.getLastFavorites();
        return Either.right(localResult);
      } on CacheException {
        return Either.left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> addToFavorites({required String cryptoSymbol}) async {
    if (await networkInfo.isConnected) {
      try {
          final favoriteId = await remoteDataSource.addToFavorites(cryptoSymbol: cryptoSymbol);
          localDataSource.addToFavorites(favoriteCryptoId: favoriteId, cryptoSymbol: cryptoSymbol);
          return const Either.right(true);
      } on ServerException {
        return Either.left(ServerFailure());
      } on CacheException {
        return Either.left(CacheFailure());
      }
    } else {
      return Either.left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> removeFromFavorites({required String cryptoId}) async {
    if (await networkInfo.isConnected) {
      try {
        final id = await localDataSource.getIdFromSymbol(symbol: cryptoId);
        if(id != null){
          await remoteDataSource.removeFromFavorites(cryptoSymbol: id);
          localDataSource.removeFromFavorites(favoriteCryptoId: id);
          return const Either.right(true);
        } else {
          return Either.left(ServerFailure());
        }
      } on ServerException {
        return Either.left(ServerFailure());
      } on CacheException {
        return Either.left(CacheFailure());
      }
    } else {
      return Either.left(ServerFailure());
    }
  }
}
