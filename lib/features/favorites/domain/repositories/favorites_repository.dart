import 'package:crypto_app/core/errors/failures.dart';
import 'package:dfunc/dfunc.dart';

abstract class FavoritesRepository {
  Future<Either<Failure, List<String>>> getFavorites();
  Future<Either<Failure, bool>> addToFavorites({required String cryptoSymbol,});
  Future<Either<Failure, bool>> removeFromFavorites({required String cryptoId});

}
