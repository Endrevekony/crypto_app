import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/favorites/domain/repositories/favorites_repository.dart';
import 'package:dfunc/dfunc.dart';

class GetFavoritesUseCase implements UseCase<List<String>, NoParams> {
  final FavoritesRepository repository;

  GetFavoritesUseCase(this.repository);

  @override
  Future<Either<Failure, List<String>>> call(NoParams params) async {
    return await repository.getFavorites();
  }
}