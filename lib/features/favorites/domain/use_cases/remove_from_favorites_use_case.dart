import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/favorites/domain/repositories/favorites_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class RemoveFromFavoritesUseCase implements UseCase<bool, RemoveFavoritesParams> {
  final FavoritesRepository repository;

  RemoveFromFavoritesUseCase(this.repository);

  @override
  Future<Either<Failure, bool>> call(RemoveFavoritesParams params) async {
    return await repository.removeFromFavorites(cryptoId: params.cryptoId,);
  }
}

class RemoveFavoritesParams extends Equatable {
  final String cryptoId;

  const RemoveFavoritesParams({
    required this.cryptoId,
  });

  @override
  List<Object> get props => [
    cryptoId,
  ];
}
