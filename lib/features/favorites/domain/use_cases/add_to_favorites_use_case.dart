import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/favorites/domain/repositories/favorites_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class AddToFavoritesUseCase implements UseCase<bool, GetFavoritesParams> {
  final FavoritesRepository repository;

  AddToFavoritesUseCase(this.repository);

  @override
  Future<Either<Failure, bool>> call(GetFavoritesParams params) async {
    return await repository.addToFavorites(cryptoSymbol: params.cryptoName,);
  }
}

class GetFavoritesParams extends Equatable {
  final String cryptoName;

  const GetFavoritesParams({
    required this.cryptoName,
  });

  @override
  List<Object> get props => [
        cryptoName,
      ];
}
