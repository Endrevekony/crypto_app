import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/login/domain/repositories/login_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class VerifySmsUseCase implements UseCase<bool, SmsVerificationParams> {
  final LoginRepository repository;

  VerifySmsUseCase(this.repository);

  @override
  Future<Either<Failure, bool>> call(SmsVerificationParams params) async {
    return await repository.verifySms(sms: params.sms);
  }
}

class SmsVerificationParams extends Equatable {
  final String sms;

  const SmsVerificationParams({
    required this.sms,
  });

  @override
  List<Object> get props => [
        sms,
      ];
}
