import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/login/domain/repositories/login_repository.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

class AuthenticatePhoneUseCase implements UseCase<bool, AuthenticationParams> {
  final LoginRepository repository;

  AuthenticatePhoneUseCase(this.repository);

  @override
  Future<Either<Failure, bool>> call(AuthenticationParams params) async {
    return await repository.verifyPhoneNumber(phoneNumber: params.phoneNumber);
  }
}

class AuthenticationParams extends Equatable {
  final String phoneNumber;

  const AuthenticationParams({
    required this.phoneNumber,
  });

  @override
  List<Object> get props => [
        phoneNumber,
      ];
}
