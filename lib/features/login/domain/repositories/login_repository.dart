import 'package:crypto_app/core/errors/failures.dart';
import 'package:dfunc/dfunc.dart';

abstract class LoginRepository {
  Future<Either<Failure, bool>> verifyPhoneNumber({required phoneNumber});
  Future<Either<Failure, bool>> verifySms({required sms});
}
