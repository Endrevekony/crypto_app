import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../../injection_container.dart';

class PhoneVerificationForm extends StatelessWidget {
  final String phoneNumber;
  final TextEditingController _phoneNumberController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  PhoneVerificationForm({required this.phoneNumber});

  @override
  Widget build(BuildContext context) {
    _phoneNumberController.text = phoneNumber;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: SizedBox(
                  height: 100,
                  width: 100,
                  child: Image.asset('lib/core/assets/images/appIcon.png'),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
               Center(
                child: Text(
                  KStrings.verifyYourPhone.tr(),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: TextFormField(
                  cursorColor: KColors.darkPurple,
                  controller: _phoneNumberController,
                  keyboardType: TextInputType.phone,
                  validator: (value) => (_validateNumber(phoneNumber: value)),
                  decoration: InputDecoration(
                    hintText: KStrings.pleaseenterYourNumber.tr(),
                    focusColor: KColors.appPurple,
                    fillColor: KColors.appPurple,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              FloatingActionButton.extended(
                backgroundColor: KColors.appPurple,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    sl<LoginBloc>().add(LoginWithPhoneNumber(
                        phoneNumber: _phoneNumberController.text));
                  }
                },
                label: Text(
                  KStrings.sendVerificationCode.tr(),
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String? _validateNumber({required String? phoneNumber}) {
    if (phoneNumber.toString().isEmpty) {
      return KStrings.fieldCannotBeEmpty;
    } else {
      return null;
    }
  }
}
