import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/widgets/error_widget.dart';
import 'package:crypto_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:string_validator/string_validator.dart';

import '../../../../injection_container.dart';

class SmsConfirmationPage extends StatefulWidget {
  final String phoneNumber;

  const SmsConfirmationPage({required this.phoneNumber});

  @override
  _SmsConfirmationPageState createState() => _SmsConfirmationPageState();
}

class _SmsConfirmationPageState extends State<SmsConfirmationPage> {
  final TextEditingController _smsController = TextEditingController();
  final _smsFormKey = GlobalKey<FormState>();
  static int attempts = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Form(
            key: _smsFormKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                const Center(
                  child: Icon(
                    FontAwesomeIcons.sms,
                    color: KColors.appPurple,
                    size: 50,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    KStrings.insertSmsNumber.tr(),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    cursorColor: KColors.darkPurple,
                    controller: _smsController,
                    keyboardType: TextInputType.number,
                    validator: (value) => ((value == null)
                        ? KStrings.fieldCannotBeEmpty
                        : (isNumeric(value) ? null : KStrings.onlyNumbers)),
                    decoration: InputDecoration(
                      hintText: KStrings.theCodeYouReceived.tr(),
                      focusColor: KColors.appPurple,
                      fillColor: KColors.appPurple,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                FloatingActionButton.extended(
                  backgroundColor: KColors.darkPurple,
                  onPressed: () {
                    if (_smsFormKey.currentState!.validate()) {
                      sl<LoginBloc>().add(
                        VerifyCode(sms: _smsController.text),
                      );
                    }
                  },
                  label: Text(
                    KStrings.verify.tr(),
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                (attempts == 3)
                    ? AppErrorWidget(errorMessage: KStrings.noMoreAttempts.tr())
                    : FloatingActionButton.extended(
                        backgroundColor: Colors.white,
                        onPressed: () {
                          attempts++;
                          sl<LoginBloc>().add(LoginWithPhoneNumber(
                              phoneNumber: widget.phoneNumber));
                          setState(() {});
                        },
                        label: Text(
                          KStrings.resendCode.tr(),
                          style: const TextStyle(
                              fontSize: 16, color: KColors.darkPurple),
                        ),
                      ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: IconButton(
              color: KColors.darkPurple,
              onPressed: () {
                sl<LoginBloc>().add(
                  GoBackToPhone(phoneNumber: widget.phoneNumber),
                );
              },
              icon: const Icon(
                FontAwesomeIcons.arrowLeft,
              ),
            ),
          )
        ],
      ),
    );
  }
}
