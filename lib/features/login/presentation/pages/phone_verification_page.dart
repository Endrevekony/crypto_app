import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/widgets/main_navigation_page.dart.dart';
import 'package:crypto_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:crypto_app/features/login/presentation/pages/sms_confirmation_page.dart';
import 'package:crypto_app/features/login/presentation/widgets/phone_verification_form.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class PhoneVerificationPage extends StatelessWidget {
  static String phoneNumber = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: BlocListener<LoginBloc, LoginState>(
            bloc: sl<LoginBloc>(),
            listener: (context, state) {
              if (state is PhoneIsVerified) {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => MainNavigationPage(
                      userPhoneNumber: FirebaseAuth
                          .instance.currentUser!.phoneNumber
                          .toString(),
                      userId: FirebaseAuth.instance.currentUser!.uid.toString(),
                    ),
                  ),
                );
              }
            },
            child: BlocBuilder<LoginBloc, LoginState>(
              bloc: sl<LoginBloc>(),
              builder: (context, state) {
                if (state is LoadingLogin) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                } else if (state is CanVerifyPhone) {
                  phoneNumber = state.phoneNumber;
                  return SmsConfirmationPage(
                    phoneNumber: phoneNumber,
                  );
                } else if (state is NoLogin) {
                  return PhoneVerificationForm(
                    phoneNumber: phoneNumber,
                  );
                } else if (state is PhoneNumberChange) {
                  phoneNumber = state.phoneNumber;
                  return PhoneVerificationForm(
                    phoneNumber: phoneNumber,
                  );
                } else if (state is LoginError) {
                  SchedulerBinding.instance!.addPostFrameCallback((_) {
                    ScaffoldMessenger.of(context).showSnackBar(
                       SnackBar(
                        backgroundColor: KColors.errorRed,
                        content: Text(KStrings.loginException.tr()),
                      ),
                    );
                  });
                  if (phoneNumber != '') {
                    return SmsConfirmationPage(phoneNumber: phoneNumber);
                  } else {
                    return PhoneVerificationForm(phoneNumber: phoneNumber);
                  }
                }
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
