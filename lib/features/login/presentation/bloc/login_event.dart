part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginWithPhoneNumber extends LoginEvent {
  final String phoneNumber;

  LoginWithPhoneNumber({required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];
}

class VerifyCode extends LoginEvent {
  final String sms;

  VerifyCode({required this.sms});

  @override
  List<Object> get props => [sms];
}


class GoBackToPhone extends LoginEvent {
  final String phoneNumber;

  GoBackToPhone({required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];
}