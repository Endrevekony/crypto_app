part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  @override
  List<Object> get props => [];
}

class NoLogin extends LoginState {}

class LoadingLogin extends LoginState {}

class CanVerifyPhone extends LoginState {
  final String phoneNumber;

  CanVerifyPhone({required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];
}

class PhoneIsVerified extends LoginState {}

class LoginError extends LoginState {
  final String message;

  LoginError({required this.message});

  @override
  List<Object> get props => [message];
}

class PhoneNumberChange extends LoginState {
  final String phoneNumber;

  PhoneNumberChange({required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];
}
