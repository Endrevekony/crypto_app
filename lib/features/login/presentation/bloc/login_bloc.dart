import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/login/domain/use_cases/authenticate_phone_use_case.dart';
import 'package:crypto_app/features/login/domain/use_cases/verify_sms_use_case.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthenticatePhoneUseCase authenticatePhone;
  final VerifySmsUseCase verifySms;

  LoginBloc({
    required this.authenticatePhone,
    required this.verifySms,
  }) : super(NoLogin());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginWithPhoneNumber) {
      yield LoadingLogin();
      final failureOrLogin = await authenticatePhone(
        AuthenticationParams(phoneNumber: event.phoneNumber),
      );
      yield* _eitherAuthenticationOrErrorState(
          failureOrLogin, event.phoneNumber);
    }
    if (event is VerifyCode) {
      yield LoadingLogin();
      final failureOrVerifiedPhone =
          await verifySms(SmsVerificationParams(sms: event.sms));
      yield* _eitherLoggedinOrErrorState(failureOrVerifiedPhone);
    }
    if (event is GoBackToPhone) {
      yield PhoneNumberChange(phoneNumber: event.phoneNumber);
    }
  }

  Stream<LoginState> _eitherAuthenticationOrErrorState(
      Either<Failure, bool> either, String phoneNumber) async* {
    yield either.fold(
        (failure) => LoginError(message: _mapFailureToMessage(failure)),
        (user) => CanVerifyPhone(phoneNumber: phoneNumber));
  }

  Stream<LoginState> _eitherLoggedinOrErrorState(
    Either<Failure, bool> either,
  ) async* {
    yield either.fold(
      (failure) => LoginError(message: _mapFailureToMessage(failure)),
      (user) => PhoneIsVerified(),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return KStrings.serverException.tr();
      case CacheFailure:
        return KStrings.cacheException.tr();
      case LoginFailure:
        return KStrings.loginException.tr();
      default:
        return KStrings.unexpectedException.tr();
    }
  }
}
