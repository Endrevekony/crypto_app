import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/features/login/data/data_sources/local/login_local_data_source.dart';
import 'package:crypto_app/features/login/domain/repositories/login_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/network_info.dart';
import 'package:dfunc/dfunc.dart';

class LoginRepositoryImpl implements LoginRepository {
  final FirebaseAuth auth;
  final LoginLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  LoginRepositoryImpl(
      {required this.networkInfo,
      required this.auth,
      required this.localDataSource});

  @override
  Future<Either<Failure, bool>> verifyPhoneNumber(
      {required phoneNumber}) async {
    if (await networkInfo.isConnected) {
      auth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: (PhoneAuthCredential credential) {},
        verificationFailed: (FirebaseAuthException e) {
          print(e.message);
        },
        codeSent: (String verificationId, int? resendToken) {
          localDataSource.cacheVerificationId(verificationId: verificationId);
        },
        codeAutoRetrievalTimeout: (String verificationId) {},
      );
      return const Either.right(true);
    } else {
      return Either.left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> verifySms({required sms}) async {
    if (await networkInfo.isConnected) {
      try {
        final String verificationId = await localDataSource.getVerificationId();
        final AuthCredential credential = PhoneAuthProvider.credential(
          verificationId: verificationId,
          smsCode: sms,
        );
        final userCredential = await auth.signInWithCredential(credential);
        final User? user = userCredential.user;
        if (user != null) {
          localDataSource.cacheUserId(userId: user.uid);
          if (user.phoneNumber != null) {
            localDataSource.cacheUserPhoneNumber(
                phoneNumber: user.phoneNumber.toString());
          }
          return const Either.right(true);
        } else {
          return Either.left(LoginFailure());
        }
      } on CacheException {
        return Either.left(CacheFailure());
      } on FirebaseAuthException {
        return Either.left(LoginFailure());
      }
    } else {
      return Either.left(ServerFailure());
    }
  }
}
