import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class LoginLocalDataSource {
  Future<String> getUserId();

  Future<void> cacheUserId(
      {required String userId});

  Future<String> getVerificationId();

  Future<void> cacheVerificationId(
      {required String verificationId});

  Future<String> getUserPhoneNumber();

  Future<void> cacheUserPhoneNumber(
      {required String phoneNumber});

}

const cachedUserId = 'CACHED_USER_ID';
const cachedVerificationId = 'CACHED_VERIFICATION_ID';
const cachedPhoneNumber = 'CACHED_PHONE_NUMBER';


class LoginLocalDataSourceImpl implements LoginLocalDataSource {
  final SharedPreferences sharedPreferences;

  LoginLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<String> getUserId() {
    final userId = sharedPreferences.getString(cachedUserId);
    if (userId != null) {
      return Future.value(userId);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheUserId({required String userId}) {
    return sharedPreferences.setString(cachedUserId, userId);
  }

  @override
  Future<void> cacheVerificationId({required String verificationId}){
    return sharedPreferences.setString(cachedVerificationId, verificationId);
  }

  @override
  Future<String> getVerificationId() {
    final verificationId = sharedPreferences.getString(cachedVerificationId);
    if (verificationId != null) {
      return Future.value(verificationId);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheUserPhoneNumber({required String phoneNumber}) {
    return sharedPreferences.setString(cachedPhoneNumber, phoneNumber);
  }

  @override
  Future<String> getUserPhoneNumber() {
    final phoneNumber = sharedPreferences.getString(cachedPhoneNumber);
    if (phoneNumber != null) {
      return Future.value(phoneNumber);
    } else {
      throw CacheException();
    }
  }
}
