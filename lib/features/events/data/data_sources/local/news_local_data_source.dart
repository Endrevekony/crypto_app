import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class NewsLocalDataSource {
  Future<NewsDataModel> getLastNews();

  Future<void> cacheNews(NewsDataModel model);
}

const cachedEvents = 'CACHED_EVENTS';

class NewsLocalDataSourceImpl implements NewsLocalDataSource {
  final SharedPreferences sharedPreferences;

  NewsLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<NewsDataModel> getLastNews() {
    final jsonString = sharedPreferences.getString(cachedEvents);
    if (jsonString != null) {
      return Future.value(eventDataModelFromJson(jsonString));
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheNews(NewsDataModel model) {
    return sharedPreferences.setString(
        cachedEvents, eventDataModelToJson(model));
  }
}
