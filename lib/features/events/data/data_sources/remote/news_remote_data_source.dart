import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:http/http.dart' as http;

abstract class NewsRemoteDataSource {
  Future<NewsDataModel> getAllEvents();
}

class NewsRemoteDataSourceImpl implements NewsRemoteDataSource {
  final http.Client client;

  NewsRemoteDataSourceImpl({required this.client});

  @override
  Future<NewsDataModel> getAllEvents() async {
    const serverUrl = KUrls.getNews;
    final response = await client.get(
      Uri.parse(serverUrl),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      final newsList = eventDataModelFromJson(response.body);
      return newsList;
    } else {
      return throw ServerException();
    }
  }
}
