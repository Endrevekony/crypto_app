import 'package:crypto_app/core/errors/exceptions.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/network_info.dart';
import 'package:crypto_app/features/events/data/data_sources/local/news_local_data_source.dart';
import 'package:crypto_app/features/events/data/data_sources/remote/news_remote_data_source.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:crypto_app/features/events/domain/repositories/news_repository.dart';
import 'package:dfunc/dfunc.dart';

class NewsRepositoryImpl implements NewsRepository {
  final NewsRemoteDataSource remoteDataSource;
  final NewsLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  NewsRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, NewsDataModel>> getNews() async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getAllEvents();
        localDataSource.cacheNews(result);
        return Either.right(result);
      } on ServerException {
        return Either.left(ServerFailure());
      }
    } else {
      try {
        final localResult = await localDataSource.getLastNews();
        return Either.right(localResult);
      } on CacheException {
        return Either.left(CacheFailure());
      }
    }
  }
}
