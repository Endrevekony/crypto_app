import 'dart:convert';

import 'package:crypto_app/features/events/domain/entities/news.dart';

NewsDataModel eventDataModelFromJson(String str) => NewsDataModel.fromJson(json.decode(str));

String eventDataModelToJson(NewsDataModel data) => json.encode(data.toJson());

class NewsDataModel {
  NewsDataModel({
    required this.data,
    required this.count,
    required this.page,
  });

  final List<News> data;
  final int count;
  final int page;

  factory NewsDataModel.fromJson(Map<String, dynamic> json) => NewsDataModel(
    data: List<News>.from(json["data"].map((x) => News.fromJson(x))),
    count: json["count"],
    page: json["page"],
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "count": count,
    "page": page,
  };
}

