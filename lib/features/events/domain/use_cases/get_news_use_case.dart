import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:crypto_app/features/events/domain/repositories/news_repository.dart';
import 'package:dfunc/dfunc.dart';

class GetNewsUseCase implements UseCase<NewsDataModel, NoParams> {
  final NewsRepository repository;

  GetNewsUseCase(this.repository);

  @override
  Future<Either<Failure, NewsDataModel>> call(NoParams params) async {
    return await repository.getNews();
  }
}
