import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:dfunc/dfunc.dart';

abstract class NewsRepository {
  Future<Either<Failure, NewsDataModel>> getNews();
}
