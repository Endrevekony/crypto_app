import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_app/features/events/domain/entities/news.dart';
import 'package:crypto_app/features/events/presentation/pages/news_item_page.dart';
import 'package:flutter/material.dart';

class NewsWidget extends StatelessWidget {
  final News item;

  const NewsWidget({required this.item});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => NewsItemPage(item: item)));
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: CachedNetworkImage(
                  imageUrl: item.screenshot,
                  width: MediaQuery.of(context).size.width / 1.2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: Column(
                  children: [
                    Text(
                      item.title,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold),
                    ),
                    Text("${item.description.substring(0, 40)} ...")
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
