import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:crypto_app/features/events/domain/entities/news.dart';
import 'package:crypto_app/features/events/presentation/widgets/news_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class NewsList extends StatefulWidget {
  final NewsDataModel model;

  const NewsList({required this.model});

  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Center(
            child: Text(
              KStrings.events.tr(),
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: widget.model.data.length,
            itemBuilder: (context, index) {
              final News item = widget.model.data[index];
              return NewsWidget(
                item: item,
              );
            },
          )
        ],
      ),
    );
  }
}
