import 'package:crypto_app/core/widgets/error_widget.dart';
import 'package:crypto_app/features/events/presentation/bloc/news_bloc.dart';
import 'package:crypto_app/features/events/presentation/widgets/news_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../injection_container.dart';

class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage>
    with AutomaticKeepAliveClientMixin<EventsPage> {

  @override
  Widget build(BuildContext context) {
    super.build(context);
    RefreshController _refreshController =
    RefreshController(initialRefresh: false);
    return BlocBuilder<NewsBloc, NewsState>(builder: (context, state) {
      if (state is EmptyNews) {
        sl<NewsBloc>().add(GetNews());
        return const Center(
          child: CircularProgressIndicator.adaptive(),
        );
      }
      if (state is LoadedNews) {
        return SmartRefresher(
          controller: _refreshController,
          header: const MaterialClassicHeader(),
          onRefresh: () {
            sl<NewsBloc>().add(GetNews());
            _refreshController.refreshCompleted();
            setState(() {});
          },
          child: NewsList(
            model: state.news,
          ),
        );
      }
      if (state is LoadingNews) {
        return const Center(
          child: CircularProgressIndicator.adaptive(),
        );
      }
      if (state is NewsError) {
        return AppErrorWidget(errorMessage: state.message);
      }
      return const Center(
        child: CircularProgressIndicator.adaptive(),
      );
    });
  }

  @override
  bool get wantKeepAlive => true;
}
