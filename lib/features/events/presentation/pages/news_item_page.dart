import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/features/events/domain/entities/news.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewsItemPage extends StatelessWidget {
  final News item;

  const NewsItemPage({required this.item});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          item.title,
          style: const TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.keyboard_backspace_sharp,
            color: Colors.white,
          ),
          onPressed: () => (Navigator.pop(context)),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: [
          CachedNetworkImage(imageUrl: item.screenshot),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Center(
              child: Text(
                item.title,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Center(
            child: Text(
              "${item.startDate.toString().substring(0, 10)} - ${item.endDate.toString().substring(0, 10)}",
              style: const TextStyle(color: KColors.darkPurple, fontSize: 14),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(item.description),
          ),
          const SizedBox(height: 10,),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                const Text("website : ",  style: TextStyle(color: KColors.darkPurple, fontSize: 14),),
                Text(item.website, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                const Text("Contact : ",  style: TextStyle(color: KColors.darkPurple, fontSize: 14),),
                Text(item.email, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                const Text("Organizer : ",  style: TextStyle(color: KColors.darkPurple, fontSize: 14),),
                Text(item.organizer, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
              ],
            ),
          )
        ],
      ),
    );
  }
}
