import 'package:bloc/bloc.dart';
import 'package:crypto_app/core/errors/failures.dart';
import 'package:crypto_app/core/strings.dart';
import 'package:crypto_app/core/usecase.dart';
import 'package:crypto_app/features/events/data/models/news_data_model.dart';
import 'package:crypto_app/features/events/domain/use_cases/get_news_use_case.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';

part 'news_event.dart';
part 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  final GetNewsUseCase getNews;
  NewsBloc({required this.getNews}) : super(EmptyNews());

  @override
  Stream<NewsState> mapEventToState(
    NewsEvent event,
  ) async* {
    if (event is GetNews) {
      yield LoadingNews();
      final failureOrNews = await getNews(NoParams());
      yield* _eitherNewsOrErrorState(failureOrNews);
    }
  }

  Stream<NewsState> _eitherNewsOrErrorState(
    Either<Failure, NewsDataModel> either,
  ) async* {
    yield either.fold(
      (failure) => NewsError(message: _mapFailureToMessage(failure)),
      (news) => LoadedNews(news: news),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return KStrings.serverException.tr();
      case CacheFailure:
        return KStrings.cacheException.tr();
      default:
        return KStrings.unexpectedException.tr();
    }
  }
}
