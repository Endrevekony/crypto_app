part of 'news_bloc.dart';

abstract class NewsState extends Equatable {
  @override
  List<Object> get props => [];
}

class EmptyNews extends NewsState {}

class LoadingNews extends NewsState {}

class LoadedNews extends NewsState {
  final NewsDataModel news;

  LoadedNews({required this.news});

  @override
  List<Object> get props => [news];
}

class NewsError extends NewsState {
  final String message;

  NewsError({required this.message});

  @override
  List<Object> get props => [message];
}
