import 'package:crypto_app/core/colors.dart';
import 'package:crypto_app/features/crypto_list/presentation/pages/list_page.dart';
import 'package:crypto_app/features/drawer/presentation/widgets/app_drawer.dart';
import 'package:crypto_app/features/events/presentation/pages/events_page.dart';
import 'package:crypto_app/features/favorites/presentation/pages/favorites_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../strings.dart';

class MainNavigationPage extends StatefulWidget {
  final String userId;
  final String userPhoneNumber;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  MainNavigationPage({required this.userId, required this.userPhoneNumber});

  @override
  _MainNavigationPageState createState() => _MainNavigationPageState();
}

class _MainNavigationPageState extends State<MainNavigationPage> {
  final PageController _myPageController = PageController(initialPage: 0);
  int _selected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget.scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          KStrings.appName,
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.menu,
            color: Colors.white,
          ),
          onPressed: () => (widget.scaffoldKey.currentState!.openDrawer()),
        ),
      ),
      drawer: AppDrawer(
        userPhoneNumber: widget.userPhoneNumber, scaffoldKey: widget.scaffoldKey,
      ),
      body: SafeArea(
        child: PageView(
          controller: _myPageController,
          onPageChanged: (index) {
            _selected = index;
            setState(() {});
          },
          children: [
            ListPage(),
            FavoritesPage(
              userId: widget.userId,
            ),
            EventsPage(),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: SizedBox(
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: SizedBox(
                  height: 50,
                  width: MediaQuery.of(context).size.width / 3,
                  child: Center(
                    child: Text(
                      KStrings.list.tr(),
                      style: TextStyle(
                        fontWeight: (_selected == 0)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        color: (_selected == 0)
                            ? KColors.darkPurple
                            : KColors.appPurple,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    _selected = 0;
                      Future.delayed(const Duration(milliseconds: 200), (){
                      _myPageController.jumpToPage(0);
                    });
                  });
                },
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: SizedBox(
                  height: 50,
                  width: MediaQuery.of(context).size.width / 3,
                  child: Center(
                    child: Text(
                      KStrings.favorites.tr(),
                      style: TextStyle(
                        fontWeight: (_selected == 1)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        color: (_selected == 1)
                            ? KColors.darkPurple
                            : KColors.appPurple,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    _selected = 1;
                    Future.delayed(const Duration(milliseconds: 200), (){
                      _myPageController.jumpToPage(1);
                    });
                  });
                },
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: SizedBox(
                  height: 50,
                  width: MediaQuery.of(context).size.width / 3,
                  child: Center(
                    child: Text(
                      KStrings.events.tr(),
                      style: TextStyle(
                        fontWeight: (_selected == 2)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        color: (_selected == 2)
                            ? KColors.darkPurple
                            : KColors.appPurple,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    _selected = 2;
                    Future.delayed(const Duration(milliseconds: 200), () {
                      _myPageController.jumpToPage(2);
                    });
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
