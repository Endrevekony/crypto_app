import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../colors.dart';

class AppErrorWidget extends StatelessWidget {
  final String errorMessage;

  const AppErrorWidget({required this.errorMessage});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const SizedBox(
            height: 50,
            width: 50,
            child: Icon(
              FontAwesomeIcons.exclamationTriangle, color: KColors.errorRed,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16,),
          ),
        ],
      ),
    );
  }
}
