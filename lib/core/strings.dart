
class KStrings {
  static const String appName = "Crypto App";
  static const String skip = "skip";
  static const String onBoardingTitle1 = "onBoardingTitle1";
  static const String onBoardingTitle2 = "onBoardingTitle2";
  static const String onBoardingTitle3 = "onBoardingTitle3";
  static const String onBoardingDescription1 = "onBoardingDescription1";
  static const String onBoardingDescription2 = "onBoardingDescription2";
  static const String onBoardingDescription3 = "onBoardingDescription3";
  static const String search = "search";
  static const String serverException = "serverException";
  static const String cacheException = "cacheException";
  static const String unexpectedException = "unexpectedException";
  static const String noSearchResults = "noSearchResults";
  static const String logOut = "logOut";
  static const String languages = "languages";
  static const String english = "English";
  static const String hungarian = "Magyar";
  static const String favorites = "favorites";
  static const String list = "list";
  static const String events = "events";
  static const String verifyYourPhone = "verifyYourPhone";
  static const String pleaseenterYourNumber = "pleaseenterYourNumber";
  static const String onlyNumbers = "onlyNumbers";
  static const String sendVerificationCode = "sendVerificationCode";
  static const String fieldCannotBeEmpty = "fieldCannotBeEmpty";
  static const String loginException = "loginException";
  static const String insertSmsNumber = "insertSmsNumber";
  static const String theCodeYouReceived = "theCodeYouReceived";
  static const String verify = "verify";
  static const String resendCode = "resendCode";
  static const String noFavoritesException = "noFavoritesException";
  static const String noDataFound = "noDataFound";
  static const String volume = "volume";
  static const String priceChange = "priceChange";
  static const String marketCapChange = "marketCapChange";
  static const String noMoreAttempts = "noMoreAttempts";
  static const String noHistory = "noHistory";
  static const String tooMuchRequestException = "tooMuchRequestException";

}

class KApiKeys {
  static const String nomicsApiKey = "8ac457d1525673d946538e66a914fab5b7d879ad";
}

class KUrls {
  static const String getNews = "http://api.coingecko.com/api/v3/events";
  static const String databaseUrl = "http://cryptoapp-64dd0-default-rtdb.europe-west1.firebasedatabase.app/";

  String getAllItems({required int page}) {
    return "http://api.nomics.com/v1/currencies/ticker?per-page=10&page=$page&key=${KApiKeys.nomicsApiKey}&sort=rank";
  }

  String searchCryptos({required String keyword}) {
    return "http://api.nomics.com/v1/currencies/ticker?ids=$keyword&key=${KApiKeys.nomicsApiKey}&sort=rank";
  }

  String getHistoryUrl({required String cryptoSymbol, required String startDate, required String isUsd}) {
    return "http://api.nomics.com/v1/currencies/sparkline?key=${KApiKeys.nomicsApiKey}&ids=$cryptoSymbol&start=$startDate&convert=$isUsd";
  }
}