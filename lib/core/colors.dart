import 'package:flutter/material.dart';

class KColors {
  static const Map<int, Color> _colorCodes = {
    50: Color.fromRGBO(179, 157, 219, .1),
    100: Color.fromRGBO(179, 157, 219, .2),
    200: Color.fromRGBO(179, 157, 219, .3),
    300: Color.fromRGBO(179, 157, 219, .4),
    400: Color.fromRGBO(179, 157, 219, .5),
    500: Color.fromRGBO(179, 157, 219, .6),
    600: Color.fromRGBO(179, 157, 219, .7),
    700: Color.fromRGBO(179, 157, 219, .8),
    800: Color.fromRGBO(179, 157, 219, .9),
    900: Color.fromRGBO(179, 157, 219, 1),
  };
  static const MaterialColor appPurple =
  MaterialColor(0xFFB39DDB, KColors._colorCodes);
  static const Color lightPurple = Color.fromRGBO(229, 206, 255, 1);
  static const Color errorRed = Color.fromRGBO(255, 0, 52, 1);
  static const Color darkPurple = Color.fromRGBO(131, 111, 169, 1);
}
