class ServerException implements Exception {}

class CacheException implements Exception {}

class LoginException implements Exception {}

class NoFavoritesException implements Exception {}

class TooMuchRequestException implements Exception {}